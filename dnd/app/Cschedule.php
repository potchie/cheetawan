<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cschedule extends Model
{
    protected $fillable = [
        'lname','mname','fname','date','start','end','status','remarks','service_id','notified'
    ];

    public function service(){
        return $this->belongsTo('App\Service');
    }

}
