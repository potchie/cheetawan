<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $fillable = [
        'name','alias', 'in', 'out', 'is_active'
    ];

    public function schedule(){
        return $this->hasMany('App\Pschedule');
    }
}
