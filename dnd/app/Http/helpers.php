<?php

use App\Cschedule;
use App\Pschedule;
use App\Shift;
use App\Personnel;
use App\Service;
use App\Role;
use Illuminate\Support\Facades\DB;
// use Carbon\Carbon;

class Helper{

	public static function getRole($value='')
	{
		$role = Role::whereStatus(1)->get();
		return $role;
	}

	public static function getHomeStat($type)
	{
		$count = 0;
		if($type == 0){
			$count = Cschedule::whereStatus(1)->count();
		}elseif($type == 1){
			$count = Cschedule::count();
		}elseif($type ==2){
			$count = Cschedule::whereStatus(0)->count();
		}

		return $count;

	}
	public static function getNewBookItems()
	{
		$count = Cschedule::whereStatus(0)->get();
		return $count;
	}

	public static function getNewBook()
	{
		$count = Cschedule::whereStatus(0)->count();
		return $count;
	}

	public static function getSched($id,$day,$month) {
		$date = date('Y-m-d',strtotime('2020-'.$month.'-'.$day));
		$sched = Pschedule::where('personnel_id',$id)->where('date',$date)->with('shift')->first();
		return $sched;
	}

	public static function getServices(){
		$services = Service::whereStatus('1')->get();
		return $services;
	}

	public static function getData($id){
		$month = "";
		if($id == 1)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('January'. date('Y'))))->count();
		elseif($id == 2)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('February'. date('Y'))))->count();
		elseif($id == 3)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('March'. date('Y'))))->count();
		elseif($id == 4)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('April'. date('Y'))))->count();
		elseif($id == 5)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('May'. date('Y'))))->count();
		elseif($id == 6)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('June'. date('Y'))))->count();
		elseif($id == 7)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('July'. date('Y'))))->count();
		elseif($id == 7)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('August'. date('Y'))))->count();
		elseif($id == 7)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('September'. date('Y'))))->count();
		elseif($id == 7)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('October'. date('Y'))))->count();
		elseif($id == 7)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('November'. date('Y'))))->count();
		elseif($id == 7)
			$month = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime('December'. date('Y'))))->count();
		else
			$month = "Potchie";
		
		return $month;
	}

	public static function getService(){
		$services = Service::whereStatus('1')->get();
		return $services;
	}

	public static function getMonth($id){
		switch ($id) {
			case 1:
				return "January";
				break;
			case 2:
				return "February";
				break;
			case 3:
				return "March";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "May";
				break;
			case 6:
				return "June";
				break;
			case 7:
				return "July";
				break;
			case 8:
				return "August";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "October";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "December";
				break;
			
			default:
				return "MONTH";
				break;
		}
	}

}