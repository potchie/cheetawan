<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZipArchive;
use File;

use App\DbBackupLog;

class DatabaseBackupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
		$data = DbBackupLog::paginate(10);
		return view('admin.database-backup', compact('data'));
    }

    public function getBackup(){

    	$data = [];
    	$filesInFolder = \File::files('backups');     
	    foreach($filesInFolder as $key=>$path) { 
	          $file = pathinfo($path);
	          $data[$key]['filename'] = $file['filename'];
	    } 

	    return $data;
    }

    public function generateBackup(){

    		$host = env("DB_HOST", "localhost");
    		$user = env("DB_USER", "root");
    		$password = env("DB_PASSWORD", "");
    		$database = env("DB_DATABASE", "cheetawan");

    		$con = mysqli_connect($host, $user, $password, $database);
			if (mysqli_connect_errno())
			{
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}else{


			    $tables = '*';
			    $return="";

			    if($tables == '*')
			    {
			        $tables = array();
			        $result = mysqli_query($con, 'SHOW TABLES');

			        while($row = mysqli_fetch_row($result))
			        {
			            $tables[] = $row[0];
			        }
			    }
			    else
			    {
			        $tables = is_array($tables) ? $tables : explode(',',$tables);
			    }
			    foreach($tables as $table)
			    {
			        $result = mysqli_query($con, 'SELECT * FROM '.$table);
			        $num_fields = mysqli_num_fields($result);
			        $return.= 'DROP TABLE IF EXISTS '.$table.';';
			        $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE '.$table));
			        $return.= "\n\n".$row2[1].";\n\n";

			        for ($i = 0; $i < $num_fields; $i++)
			        {
			            while($row = mysqli_fetch_row($result))
			            {
			                $return.= 'INSERT INTO '.$table.' VALUES(';
			                for($j=0; $j<$num_fields; $j++)
			                {
			                    $row[$j] = addslashes($row[$j]);
			                    $row[$j] = preg_replace("/(\n){2,}/", "\\n", $row[$j]);

			                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
			                    if ($j<($num_fields-1)) { $return.= ','; }
			                }
			                $return.= ");\n";
			            }
			        }
			        $return.="\n\n\n";
			    }
			    date_default_timezone_set('Asia/Taipei');
				$date = date("FdY");
				$filename = "database_backup_".$date.'_'.time();
			    $handle = fopen(storage_path('app\backup').'\\'.$filename.'.sql','w+');
			    fwrite($handle,$return);
			    fclose($handle);

				$dirPath = storage_path("app/backup")."/";
				$this->createZipFile($dirPath, $filename);

			}
    }

    public function downloadBackup($fileName){

		$filepath = storage_path('app/backup/');
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".$fileName."\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($filepath.$fileName));
		ob_end_flush();
		@readfile($filepath.$fileName);
		//unlink($fileName);
	}
	
	public function createZipFile($dirPath, $fileName){
		$zip = new ZipArchive;
   
		$zipName = $fileName.'.zip';
		$sqlFile = $fileName.'.sql';
   
        if ($zip->open($dirPath.$zipName, ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(storage_path('app/backup'));
   
            foreach ($files as $key => $value) {
				$relativeNameInZipFile = basename($value);
				if(DbBackupLog::whereFileName($relativeNameInZipFile)->count() < 1)
                $zip->addFile($value, $relativeNameInZipFile);
            }
             
            $zip->close();
		}
		unlink($dirPath.$sqlFile);
		$this->saveDbBackupLogs($zipName, $dirPath);
		$this->downloadBackup($zipName);
	}

	public function saveDbBackupLogs($backupName, $path){

		$d = [
			'file_name' => $backupName,
			'path' =>  $path
		];

		DbBackupLog::create($d);
		return;
	}
}


