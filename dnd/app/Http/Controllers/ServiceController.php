<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use View;
class ServiceController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Services']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $ps = Service::create([
            'name' => $request->name,
            'duration' => $request->duration,
            'price' => $request->price,
            'status' => 1,

        ]);

        if($ps)
            return "Success";
        else
            return "Error";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }

    public function sdeact(Request $request){
        $stat = 1;

        if($request->type == 1)
            $stat = 0;
        else
            $stat = 1;


        $update = Service::where('id',$request->id)->
        update([
            'status' => $stat
        ]);

        if($update)
            return 'Success';
        else
            return 'Error';
    }

    public function updateService(Request $request)
    {

        $update = Service::where('id',$request->id)->update([
            'name' => $request->name,
            'duration' => $request->duration,
            'price' => $request->price
        ]);

        if($update)
            return "Success";
        else
            return "Error";
    }
}
