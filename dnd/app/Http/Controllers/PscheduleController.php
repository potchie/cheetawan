<?php

namespace App\Http\Controllers;

use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Http\Request;
use App\Pschedule;
use App\Shift;
use App\Personnel;
use View;
use Auth;
class PscheduleController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Psched']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shift = Shift::where('is_active',1)->get();
        $personnel = Personnel::where('status',1)->get();
        $scheds = Pschedule::all();
        $events = [];
        foreach ($scheds as $key => $row) {
                $events[] = Calendar::event(
                $row->personnel->fname. " - " .$row->shift->alias,
                false,
                date('Y-m-d h:i A', strtotime($row->date. " ".$row->shift->in)),
                date('Y-m-d h:i A', strtotime($row->date. " ".$row->shift->out)),
                null,
                // Add color and link on event VIOLETE
                 [
                     
                     'color' => '#F4512F'
                     // 'url' => url("/"),
                 ]
                );
        }

        $calendar = Calendar::addEvents($events);
        return view('pschedule.index',compact('personnel','calendar','shift'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function saveSched(Request $request)
    {
        $error='';
        $success='';
        for($i = date('Y-m-d',strtotime($request->in)); $i<=date('Y-m-d',strtotime($request->out)); $i++)
        {
            $exist = Pschedule::where('personnel_id',$request->id)->where('date',$i)->count();
            if($exist > 0){
                $error = $error.$i."`";
            }else{
                $ps = Pschedule::create([
                    'shift_id' => $request->shift,
                    'user_id' => Auth::user()->id,
                    'personnel_id' => $request->id,
                    'date' => $i,
                ]);
                $success = $success.$i."`";
            }
        }


        return "Success|".$error."|".$success;

    }

    public function savebSched(Request $request)
    {
        $error='';
        $success='';
        foreach ($request->personnel as $key => $p_id) {
            $exist = Pschedule::where('personnel_id',$p_id)->where('date',$request->in)->count();
            $person = Personnel::whereId($p_id)->first();
            if($exist > 0){
                $error = $error.$person->lname.", ".$person->fname."`";
            }else{
                $ps = Pschedule::create([
                    'shift_id' => $request->shift,
                    'user_id' => Auth::user()->id,
                    'personnel_id' => $p_id,
                    'date' => $request->in,
                ]);
                $success = $success.$person->lname.", ".$person->fname."`";
            } 
        }
        return "Success|".$error."|".$success;
    }

    public function pSchedMany()
    {
        $shift = Shift::where('is_active',1)->get();
        $personnel = Personnel::where('status',1)->get();
        return view('pschedule.mass',compact('shift','personnel'));
    }

    public function editshed($id)
    {
        $sched =Pschedule::whereId($id)->first();
        $shift = Shift::whereIsActive(1)->get();
        return view('pschedule.edits', compact('sched','shift'));
    }

    public function editSched(Request $request)
    {
        $exist = Pschedule::where('personnel_id',$request->p_id)->where('date',$request->date)->where('id','<>',$request->id)->count();
        if($exist > 0)
        {
            return "Count";
        }else{
            $update = PSchedule::where('id',$request->id)->update([
                'shift_id' => $request->shift,
                'date' => $request->date,
            ]);

            if($update)
                return "Success";
            else
                return "Error";
        }
    }
}
