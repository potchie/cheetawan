<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personnel;
use App\Shift;
use App\Cschedule;
use DB;
use PDF;
class ReportController extends Controller
{
    //Personnel Schedule
    public function monthlysched($month){
    	// create new PDF document

	    $personnel = Personnel::where('status',1)->get();
        $shift = Shift::where('is_active',1)->get();

        PDF::SetTitle('Monthly Schedule');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::AddPage('L');
        PDF::SetFont('helvetica','8');
        
        
        $view = \View::make('reports/monthlysched', compact('personnel','month','shift'));
        $html = $view->render();
        // date('F d, Y')
        PDF::writeHTML($html,true, false, true, false, '');
        PDF::Output('Monthly Schedule.pdf','I');

        // return view('reports/monthlysched', compact('personnel','month','shift'));
    }

    // customer Schedule
    public function  monthlyCsched($month){
    	// create new PDF document
        $currdate = '2020-'.$month.'-1';
        $schedule = Cschedule::whereYear('date',date('Y-m-d'))->whereMonth('date',date('m',strtotime($currdate)))->get();

        // SELECT * FROM cschedules WHERE YEAR(date) = YEAR('2020/06/23') AND MONTH(date) = MONTH('2020/06/23')
        PDF::SetTitle('Customer Schedule of the Month');
        PDF::setPrintHeader(false);
        PDF::setPrintFooter(false);
        PDF::AddPage('L');
        PDF::SetFont('helvetica','8');
        
        
        $view = \View::make('reports/monthlyCsched', compact('schedule','month','currdate'));
        $html = $view->render();
        // date('F d, Y')
        PDF::writeHTML($html,true, false, true, false, '');
        PDF::Output('Customer Schedule.pdf','I');

        // return view('reports/monthlysched', compact('personnel','month','shift'));
    }
   
}
