<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Room;
use App\Deck;
use App\Cschedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $ps = Assignment::create([
                'cschedule_id' => $request->aoptSched,
                'room_id' => $request->aoptRoom,
                'status' => 1,
                'p_id' => $request->aid,
                'p_name' =>$request->aname
            ]);

            //0-for approval
            //1- active
            //2- on-going
            //3 - finished
            //4 - cancelled
            $sched = Cschedule::whereId($request->aoptSched)->update(['status'=>2]);//on going
            $rs = Room::whereId($request->aoptRoom)->update(['status'=>1]); //room occupied
            $max = Deck::max('id');
            $deck = Deck::wherePersonnelId($request->aid)->update(["id"=>$max+1,"status"=>0]); //move to the last and mark as not available

            if($ps and $rs and $sched and $deck){
                DB::commit();
                return "Success";
            }else{
                DB::rollback();
                return "Error|".$max;
            }

        }catch(Exception $ex){
            DB::rollback();
            return "Error|".$ex;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assignment $assignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        //
    }
}
