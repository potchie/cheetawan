<?php

namespace App\Http\Controllers;

use App\Shift;
use App\Personnel;
use Illuminate\Http\Request;
use View;
class ShiftController extends Controller
{
     public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Shift']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personnel = Shift::all();
        return view('shift.index',compact('personnel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $ps = Shift::create([
            'name' => $request->name,
            'alias'=>$request->alias,
            'in' => $request->in,
            'out' => $request->out,
            'is_active' => 1,

        ]);

        if($ps)
            return "Success";
        else
            return "Error";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(Shift $shift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shift $shift)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        //
    }

    public function deact(Request $request){
        $stat = 1;

        if($request->type == 1)
            $stat = 0;
        else
            $stat = 1;


        $update = Shift::where('id',$request->id)->
        update([
            'is_active' => $stat
        ]);

        if($update)
            return 'Success';
        else
            return 'Error';
    }

     public function updateShift(Request $request)
    {
        $update = Shift::where('id',$request->id)->update([
            'name' => $request->ename,
            'alias'=>$request->ealias,
            'in' => $request->ein,
            'out' => $request->eout,
        ]);

        if($update)
            return "Success";
        else
            return "Error";
    }

    public function getTime(Request $request)
    {
        $item = Shift::whereId($request->id)->first();

        if($item){
            $text = date('h:i a',strtotime($item->in)) . " - " . date('h:i a',strtotime($item->out));
            return $text;
        }else{
            return ("Not Found");
        }
    }
}
