<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Shift;
use App\Personnel;
use View;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'User']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('username','<>','potchie')->get();
        $roles = Role::all();
        return view('user.index',compact('users','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(User::whereUsername($request->username)->count()>0)
        {
            return "Error|Username already exists.";
        }elseif(User::whereName($request->name)->count()>0){
            return "Error|Name already exists.";
    
        }else{
             $ps = User::create([
                'username' =>$request->username,
                'name' => $request->name,
                'role_id' => $request->role,
                'status' => 1,
                'is_active' => 1,
                'password' => Hash::make('123456')

            ]);

            if($ps)
                return "Success";
            else
                return "Error";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shift = Shift::where('is_active',1)->get();
        $personnel = User::where('id',$id)->first();
        return view('personnel.profile',compact('personnel','shift'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deact(Request $request){
        $stat = 1;

        if($request->type == 1)
            $stat = 0;
        else
            $stat = 1;


        $update = User::where('id',$request->id)->
        update([
            'is_active' => $stat
        ]);

        if($update)
            return 'Success';
        else
            return 'Error';
    }

    public function resetPass(Request $request)
    {
        $update = User::whereId($request->id)->update([
            'password' => Hash::make('123456') 
        ]);

        if($update)
            return "Success";
        else
            return "Error";
    }

    public function changePass(Request $request)
    {
        $update = User::whereId($request->id)->update([
            'password' => Hash::make( $request->password )
        ]);

        if($update)
        return "Success";
        else
            return "Error";
    }

    public function checkUsername(Request $request)
    {
        $count = User::whereUsername($request->username)->count();
        return $request;

    }

    public function updateUser(Request $request)
    {
        $update = User::where('id',$request->id)->update([
            'fname' => $request->fname,
            'mname' => $request->mname,
            'lname' => $request->lname,
            'name' => $request->name,
        ]);

        if($update)
            return "Success";
        else
            return "Error";
    }
}
