<?php

namespace App\Http\Controllers;

use App\Deck;
use App\Service;
use App\Cschedule;
use App\Room;
use Illuminate\Http\Request;
use View;
use App\Shift;
use App\Personnel;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\Assignment;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Dashboard']);
        // $this->middleware('auth');
    }


    public function home()
    {

        $services = Service::whereStatus('1')->get();
        return view('welcome',compact('services'));
    }

    public function saveCSchedA(Request $request)
    {
        $minutes = Service::whereId($request->service)->pluck('duration')->first();
        $ps = Cschedule::create([
            'fname' => $request->fname,
            'mname' => $request->mname,
            'date'=> $request->date,
            'lname' => $request->lname,
            'start' => $request->start,
            'end' =>  date("H:i:s", strtotime("+".$minutes."minutes".$request->start)),
            'remarks' =>"Active",
            'status' => 1,
        ]);
        //0-for approval
        //1- active
        //2- on-going
        //3 - finished
        //4 - cancelled
        if($ps)
            return "Success";
        else
            return "Error";
        //return ($request);
    }

    public function saveCSchedH(Request $request)
    {
        $minutes = Service::whereId($request->service)->pluck('duration')->first();
        $ps = Cschedule::create([
            'fname' => $request->fname,
            'mname' => $request->mname,
            'date'=> $request->date,
            'lname' => $request->lname,
            'start' => $request->start,
            'end' =>  date("H:i:s", strtotime("+".$minutes."minutes".$request->start)),
            'remarks' =>"For reservation",
            'status' => 0,
        ]);
        //0-for approval
        //1- active
        //2- on-going
        //3 - finished
        //4 - cancelled
        if($ps)
            return "Success";
        else
            return "Error";
        //return ($request);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function getDeck()
    {
        DB::beginTransaction();
        try{
            $personnel = Personnel::whereHas('schedule', function($query){ $query->where('date',date('Y-m-d'))->whereHas('shift',function($query){$query->where('in','<=',date('H:i'))->where('out','>=',date('H:i'));});})->get();

            $currentDeck = Deck::where('status',1)->get();
            //delete old list
            foreach ($currentDeck as $key => $row) {
                if(!$personnel->has($row->id))
                    $row->delete();
            }



            //add new list
            foreach ($personnel as $key => $row) {
                if(Deck::wherePersonnelId($row->id)->count() == 0){  
                    Deck::create(['personnel_id'=>$row->id, 'status'=>1]);
                }
            }
            $personnel = Deck::orderby('id')->with('personnel')->get();
            DB::commit();
            return $personnel;
        }catch(Exemption $ex){
            DB::rollback();
            $personnel = Deck::orderby('id')->get();
            return $personnel;
        }
        
    }

    public function index()
    {
        $events = [];
        $events[] = Calendar::event(
        'Archie S. Lasalita',
        true,
        new \DateTime(date('Y-m-d')),
        new \DateTime(date('Y-m-d')),
        null,
        // Add color and link on event VIOLETE
         [
             
             'color' => '#F4512F',
             'url' => url("/"),
         ]
        );
        $calendar = Calendar::addEvents($events);
        $ass = Assignment::where('status',1)->get();
        $personnel = $this->getDeck();
        $rooms = $this->getRooms();
        $cschedule = $this->getCschedule();
        return view('home',compact('calendar','personnel','ass','rooms','cschedule'));
    }

    public function getRooms()
    {
        return Room::where('status',0)->where('is_active',1)->get();
    }

    public function getCschedule()
    {
        //0-for approval
        //1- active
        //2- on-going
        //3 - finished
        //4 - cancelled
        return Cschedule::where('date',date('Y-m-d'))->where('status',1)->with('service')->orderby('start')->get();
    }
    
}
// 