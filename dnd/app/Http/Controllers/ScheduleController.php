<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\Pschedule;
use App\Cschedule;
use App\Shift;
use App\Personnel;
use View;
use Auth;

class ScheduleController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Csched']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shift = Shift::where('is_active',1)->get();
        $personnel = Personnel::where('status',1)->get();
        $scheds = Cschedule::where('status',1)->get();
        $events = [];
        foreach ($scheds as $key => $row) {
                $events[] = Calendar::event(
                $row->lname,
                false,
                date('Y-m-d H:i ', strtotime($row->date. " ".$row->start)),
                date('Y-m-d H:i ', strtotime($row->date. " ".$row->end)),
                null,
                // Add color and link on event VIOLETE
                 [
                     
                     'color' => '#F4512F'
                     // 'url' => url("/"),
                 ]
                );
        }

        $calendar = Calendar::addEvents($events);
        $approval = Cschedule::whereStatus(0)->get();
        $today = Cschedule::whereStatus(1)->where('date',date('Y-m-d'))->get();
        return view('cschedule.index',compact('personnel','calendar','shift','approval','today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function booking()
    {
        View::share(["curpage"=>'Booking']);
        $approval = Cschedule::whereStatus(0)->with('service')->get();
        return view('cschedule.approval',compact('approval'));
    }

    public function deny(Request $request)
    {
        $update = Cschedule::where('id',$request->id)->
            update([
                'status' => 4
            ]);

        if($update)
            return "Success";
         else
            return "Error|".$update."|".$request->id;
    }

    public function verifyTime(Request $request)
    {
        //0-for approval
        //1- active
        //2- on-going
        //3 - finished
        //4 - cancelled
        $update = Cschedule::where('id',$request->vid)->
            update([
                'status' => 1,
                'start' => $request->vstart,
                'end' => $request->vend
            ]);

        if($update)
            return "Success";
         else
            return "Error";
        // return $request;
    }

    public function testme()
    {   
        //return date('Y-m-d'). " " . date('H:i:s',strtotime('14:32:00'));
        return Cschedule::whereStatus(0)->with('service')->get();
    }
}
