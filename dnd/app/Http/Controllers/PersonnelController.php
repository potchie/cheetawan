<?php

namespace App\Http\Controllers;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Http\Request;
use View;
use App\Personnel;
use App\Shift; 
use App\Pschedule;
class PersonnelController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Personnel']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personnel = Personnel::all();
        return view('personnel.index',compact('personnel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ps = Personnel::create([
            'fname' => $request->fname,
            'mname' => $request->mname,
            'lname' => $request->lname,
            'age' => $request->age,
            'gender' => $request->gender,
            'salary' => $request->salary,
            'status' => 1,

        ]);

        if($ps)
            return "Success";
        else
            return "Error";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function show($id)
    {
        $shift = Shift::where('is_active',1)->get();
        $personnel = Personnel::where('id',$id)->first();
        $scheds = Pschedule::where('personnel_id',$id)->get();
        $events = [];
        foreach ($scheds as $key => $row) {
                $events[] = Calendar::event(
                $row->shift->name,
                false,
                date('Y-m-d h:i A', strtotime($row->date. " ".$row->shift->in)),
                date('Y-m-d h:i A', strtotime($row->date. " ".$row->shift->out)),
                null,
                // Add color and link on event VIOLETE
                 [
                     
                     'color' => '#F4512F',
                     'url' => url("admin/editshed/".$row->id),
                 ]
                );
        }

        $calendar = Calendar::addEvents($events);
        return view('personnel.show',compact('personnel','calendar','shift'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function deact(Request $request){
        $stat = 1;

        if($request->type == 1)
            $stat = 0;
        else
            $stat = 1;


        $update = Personnel::where('id',$request->id)->
        update([
            'status' => $stat
        ]);

        if($update)
            return 'Success';
        else
            return 'Error';
    }


    public function updatePersonnel(Request $request)
    {

        $update = Personnel::where('id',$request->id)->update([
            'fname' => $request->fname,
            'mname' => $request->mname,
            'lname' => $request->lname,
            'age' => $request->age,
            'gender' => $request->gender,
            'salary' => $request->salary,
        ]);

        if($update)
            return "Success";
        else
            return "Error";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
