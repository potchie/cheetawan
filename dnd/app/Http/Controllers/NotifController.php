<?php
 
namespace App\Http\Controllers;

use App\Notif;
use Illuminate\Http\Request;
use Auth;
use Sse\SSE;
use Sse\Event;
class NotifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SSE $sse)
    { 
        // header('Content-Type: text/event-stream');
        // header('Cache-Control: no-cache');
        // $branch = Auth::user()->branch;

        // $notification = Notif::where(['towarehouse' => $branch,'read_status' => 'unread'])->get()->count();
        // echo "data:{$notification} \n\n";
        // flush();

        // Add your event listener
        $sse = new SSE();
        $sse->exec_limit = 5;
        $sse->addEventListener('notif',  new NotifEvent());
        return $sse->createResponse();
    

    }

    public function toText(SSE $sse2)
    {
        $sse2 = new SSE();
        $sse2->exec_limit = 5;
        $sse2->addEventListener('totext',  new TotextEvent());
        return $sse2->createResponse();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notif  $notif
     * @return \Illuminate\Http\Response
     */
    public function show(Notif $notif)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notif  $notif
     * @return \Illuminate\Http\Response
     */
    public function edit(Notif $notif)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notif  $notif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notif $notif)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notif  $notif
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notif $notif)
    {
        //
    }

    /**********ARCHIE MODIFIED ROUTES**********/
    public function notified()
    {
        $notifupdate = TRUE;
        if($notifupdate)
            return "Success";
        else
            return "Error";


    }
}

