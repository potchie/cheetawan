<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }
    
    protected function credentials(Request $request)
    {
        $data = $request->only($this->username(), 'password');
        $data['is_active'] = '1';
        return $data;
    }

    //overwrite messages
   protected function sendFailedLoginResponse(Request $request)
   {
         if ( ! User::where('username', $request->username)->first() ) {
            throw ValidationException::withMessages([   
            $this->username() => [trans('Username does not exists.')],
                ])->redirectTo('login');
         }

         if ( ! User::where('username', $request->username)->where('password', bcrypt($request->password))->where('is_active','2')->first() ) {
            throw ValidationException::withMessages([   
                'password' => [trans('Incorrect password.')],
                ])->redirectTo('login');
        }
    }
}
