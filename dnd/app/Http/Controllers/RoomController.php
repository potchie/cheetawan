<?php

namespace App\Http\Controllers;

use App\Room;
use App\Assignment;
use App\Deck;
use App\Cschedule;
use Illuminate\Http\Request;
use View;
class RoomController extends Controller
{
     public function __construct()
    {
        date_default_timezone_set('Asia/Manila');
        View::share(["curpage"=>'Rooms']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::get();
        return view('rooms.index',compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ps = Room::create([
            'name' => $request->name,
            'status' => 0,
            'is_active' => 1,
        ]);

        if($ps)
            return "Success";
        else
            return "Error";
    }

    public function rdeact(Request $request){
        $stat = 1;

        if($request->type == 1)
            $stat = 0;
        else
            $stat = 1;


        $update = Room::where('id',$request->id)->
        update([
            'is_active' => $stat
        ]);

        if($update)
            return 'Success';
        else
            return 'Error';
    }

     public function updateRoom(Request $request)
    {

        $update = Room::where('id',$request->id)->update([
            'name' => $request->name,
            
        ]);

        if($update)
            return "Success";
        else
            return "Error";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        //
    }

    public function clearRoom($id)
    {
        $assignments = Assignment::whereId($id)->first();

        Room::whereId($assignments->room_id)->update(['status'=>0]); //vacant
        Deck::wherePersonnelId($assignments->p_id)->update(['status'=>1]); //available
        Assignment::whereId($id)->update(['status'=>0]); //idle or done
        Cschedule::whereId($assignments->cschedule_id)->update(['status'=>3]); //done

        return 'Success';
    }
}
