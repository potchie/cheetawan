<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deck extends Model
{
    protected $fillable = [
        'personnel_id','status',
    ];
    
    public function personnel(){
        return $this->belongsTo('App\Personnel');
    }
}
