<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pschedule extends Model
{
    protected $fillable = [
        'personnel_id','shift_id','user_id','date',
    ];


    public function shift(){
        return $this->belongsto('App\Shift');
    }

    public function personnel(){
        return $this->belongsto('App\Personnel');
    }

    public function user(){
    	return $this->belongsto('App\User');
    }
}
