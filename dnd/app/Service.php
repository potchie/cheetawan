<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name','duration','price','status',
    ];


    public function schedule(){
        return $this->hasMany('App\Cschedule');
    }
}
