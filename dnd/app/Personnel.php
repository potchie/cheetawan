<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    //

     protected $fillable = [
        'fname', 'lname', 'mname', 'age', 'gender','salary','status',
    ];

    public function schedule(){
        return $this->hasMany('App\Pschedule');
    }

}
