<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = [
        'cschedule_id','room_id','status','p_id','p_name'
    ];

    public function cschedule(){
        return $this->belongsTo('App\Cschedule');
    }
	public function room(){
        return $this->belongsTo('App\Room');
    }

    public function personnel(){
        return $this->belongsTo('App\Personnel');
    }
}


