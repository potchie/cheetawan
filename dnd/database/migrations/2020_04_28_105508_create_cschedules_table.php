<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCschedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cschedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lname');
            $table->string('mname');
            $table->string('fname');
            $table->date('date');
            $table->time('start');
            $table->time('end');
            $table->integer('status');
            //1- active
            //2- on-going
            //3 - finished
            //4 - cancelled
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cschedules');
    }
}
