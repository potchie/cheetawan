<?php

use Illuminate\Database\Seeder;

class AddRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name'=>'Receptionnist',
            'restriction'=>'2'
        ]);
        
        DB::table('roles')->insert([
            'name'=>'Therapist',
            'restriction'=>'3'
        ]);


    }
}
