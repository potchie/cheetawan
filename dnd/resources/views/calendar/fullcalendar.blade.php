@section('extrastyle')
<link href="{{asset('fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css">
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/> -->
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
@endsection


<div class="col-md-12">
		<div class="card-title">
			<center>Calendar of Schedule</center>
		</div>
      	<div class="card-body">
                 {!! $calendar->calendar() !!}
    	</div>
            
    </div>
</div>
