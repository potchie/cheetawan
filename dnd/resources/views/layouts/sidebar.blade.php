    <div class="sidebar" data-color="red">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="{{url('/admin')}}" class="simple-text logo-mini">
          CT
        </a>
        <a href="{{url('/admin')}}" class="simple-text logo-normal">
          CHEETAWAN
        </a>
        <div class="navbar-minimize">
          <button id="minimizeSidebar" class="btn btn-simple btn-icon btn-neutral btn-round">
            <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
            <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
          </button>
        </div>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="{{asset('img/mike.png')}}" />
          </div>
          <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span>
                {{Auth::user()->name}}
                <b class="caret"></b>
              </span>
            </a>
            <div class="clearfix"></div>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li>
                  <a href="{{route('users.show',Auth::user()->id)}}">
                    <span class="sidebar-mini-icon">MP</span>
                    <span class="sidebar-normal">My Profile</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <ul class="nav">
          <li class="{{$curpage=='Dashboard'? 'active' : ''}}">
            <a href="{{url('/admin')}}">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="{{$curpage=='Personnel'? 'active' : ''}}">
            <a href="{{url('/admin/personnel')}}">
              <i class="now-ui-icons users_single-02"></i>
              <p>Personnel</p>
            </a>
          </li>
          <li class="{{$curpage=='Schedules'? 'active' : ''}}">
            <a href="#Schedules" data-toggle="collapse">
              <i class="now-ui-icons ui-1_calendar-60"></i>
              <p>
                Schedules
                <b class="caret"></b>
              </p>
            </a>
            <div class="{{$curpage=='Psched' || $curpage=='Csched' || $curpage=='Booking'? '' : 'collapse'}}" id="Schedules">
                      <ul class="nav">
                          <li class="{{$curpage=='Psched'? 'active' : ''}}">
                            <a href="{{url('/admin/Psched')}}">
                              <span class="sidebar-mini-icon">Ps</span>
                              <span class="sidebar-normal"> Personnel Schedule </span>
                            </a>
                          </li>
                          <li class="{{$curpage=='Csched'? 'active' : ''}}">
                            <a href="{{url('/admin/Csched')}}">
                              <span class="sidebar-mini-icon">Cs</span>
                              <span class="sidebar-normal"> Customer Schedule </span>
                            </a>
                          </li>
                          
                      </ul>
              </div>
          </li>
          <li {{ Auth::user()->role_id >1? 'hidden':'' }} class="{{$curpage=='Setting'? 'active' : ''}}">
            <a href="#Settings" data-toggle="collapse">
              <i class="now-ui-icons ui-1_settings-gear-63"></i>
              <p>
                Settings
                <b class="caret"></b>
              </p>
            </a>
            <div class="{{$curpage=='Shift' || $curpage=='Services' || $curpage=='Rooms'? '' : 'collapse'}}" id="Settings">
                      <ul class="nav">
                          <li class="{{$curpage=='Shift'? 'active' : ''}}">
                            <a href="{{url('/admin/shift')}}">
                              <span class="sidebar-mini-icon">Sh</span>
                              <span class="sidebar-normal"> Shift </span>
                            </a>
                          </li>
                          <li class="{{$curpage=='Services'? 'active' : ''}}">
                            <a href="{{url('/admin/services')}}">
                              <span class="sidebar-mini-icon">SC</span>
                              <span class="sidebar-normal"> Services </span>
                            </a>
                          </li>
                          <li class="{{$curpage=='Rooms'? 'active' : ''}}">
                            <a href="{{url('/admin/rooms')}}">
                              <span class="sidebar-mini-icon">RM</span>
                              <span class="sidebar-normal"> Rooms </span>
                            </a>
                          </li>
                          <li class="{{$curpage=='Backup'? 'active' : ''}}">
                            <a href="{{url('/admin/databackup')}}">
                              <span class="sidebar-mini-icon">Db</span>
                              <span class="sidebar-normal"> Backup </span>
                            </a>
                          </li>
                      </ul>
              </div>
          </li>
          <li {{ Auth::user()->role_id >1? 'hidden':'' }} class="{{$curpage=='User'? 'active' : ''}}">
            <a href="{{url('/admin/users')}}">
              <i class="now-ui-icons users_circle-08"></i>
              <p>Users</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <script>
       var source = new EventSource('{{url('/admin/toText')}}');
       source.addEventListener('totext', function(event) {
           var dt  = JSON.parse(event.data);
              console.log(dt.forText.length);
              if(dt.forText.length > 0)
              {
                  var audio = new Audio('{{asset("sounds/notif.mp3")}}');
                  audio.play();  
                  func.showNotification('bottom','right',"<b>Alert</b><br>There are currently ("+dt.forText.length+") customer scheduled an hour later. Please send them an SMS confirmation.");
              }
      }, false); 
    </script>