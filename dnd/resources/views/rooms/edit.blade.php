<div class="modal fade" id="editRoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-notice">
        <div class="modal-content">
          <form method="post" id="editRoom" onsubmit="return editRoom();">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <h5 class="modal-title" id="myModalLabel">Edit Service</h5>
          </div>
          <div class="modal-body">
            <div class="instruction">
            	<input name="eid" id="eid" type="text" hidden value="" required class="form-control" placeholder="Room Name">
	              <div class="input-group no-border">
	                <input name="ename" id="ename" type="text" value="" required class="form-control" placeholder="Room Name">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Room Name"></i>
	                  </div>
	                </div>
	              </div>
	          

	          
            </div>
            <p></p>
          </div>
          <div class="modal-footer justify-content-center">
            <button class="btn btn-info btn-round" type="submit">Update <i class="now-ui-icons ui-1_send"></i></button>
          </div>
          </form>
        </div>
      </div>
</div>