<!DOCTYPE html>
<html>
<head>
	<title>Customer Schedule</title>
	<style>
		ul
		{
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}

		li
		{
			float:left;
		}
	</style>
</head>
<body>
	<center><img width="50" height="50" src="{{asset('img/logo.jpg')}}"></center>
	<h6 align="center"> Schedule for the Month of: <u>{{Helper::getMonth($month)}}</u> </h6>
	
		<table align="center" border="1" style="font-size: xx-small;">
			<tr style="font-weight: bold; text-align: center; color: white;" bgcolor="gray">
				<th>
					Customer Name
				</th>
				<th>
					Date
				</th>
				<th>
					Service
				</th>
				<th>
					Status
				</th>
			</tr>
				@foreach($schedule as $row)
					<tr>
						<td>{{$row->fname}} {{$row->mname}}{{$row->lname}}</td>
						<td>{{date('M d, Y', strtotime($row->date))}}</td>
						<td>{{$row->service->duration}} mins.- {{$row->service->name}}</td>
						<td>
							<!-- //0-for approval
							//1- active
							//2- on-going
							//3 - finished
							//4 - cancelled -->
							@if($row->status==0)
								For approval
							@elseif($row->status==1)
								Active
							@elseif($row->status==2)
								On-going (during the generation of this report)
							@elseif($row->status==3)
								Served
							@elseif($row->status==4)
								Cancelled
							@endif
						</td>
					</tr>
				@endforeach
			
		</table>
		<br>
		<br>
		<div style="font-size: xx-small;">
			<table border="0" style="font-size: xx-small;">
				<tr>
					<td width = "70px">Prepared by:</td>
					<td><u>{{Auth::user()->fname}} {{Auth::user()->mname}} {{Auth::user()->lname}}</u></td>
				</tr>
				<tr>
					<td>Date:</td>
					<td><u>{{date('M d, Y')}}</u></td>
				</tr>
			</table>
		</div>
	
</body>
</html>