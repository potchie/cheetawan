<!DOCTYPE html>
<html>
<head>
	<title>Monthly Schedule</title>
	<style>
		ul
		{
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}

		li
		{
			float:left;
		}
	</style>
</head>
<body>
	<center><img width="50" height="50" src="{{asset('img/logo.jpg')}}"></center>
	<h6 align="center"> Schedule for the Month of: <u>{{Helper::getMonth($month)}}</u> </h6>
	<table align="center" border="1" style="font-size: xx-small;">
		<tr style="font-weight: bold; text-align: center; color: white;" bgcolor="gray">
			<th width="15%">Employee Name</th>
			@for($i = 1; $i<=31; $i++)
					<th width="2.8%">
						{{$i}}
					</th>
			@endfor
		</tr>
		@foreach($personnel as $key => $row)
			<tr>
				<td style="text-align: left;">
					{{$key+1}} {{$row->fname}} {{$row->mname[0]}}. {{$row->lname}}
				</td>
				@for($i = 1; $i<=31; $i++)
					@php $sched =  Helper::getSched($row->id, $i,$month); @endphp
					<td style="text-align: center;">
						@if($sched)
							{{ $sched['shift']['alias'] }}
						@endif
					</td>
				@endfor
			</tr>
		@endforeach
	</table>
	<br>
	<div style="font-size: xx-small;">
		Shift Legend:
		<ul>
			@foreach($shift as $row)
				<li><span style="color:gray;"> {{strtoupper($row->alias)}} ( {{$row->name}} ) </span> | <span style="color:green">{{ date('h:i a', strtotime($row->in)) }} </span> to <span style="color:red">{{ date('h:i a', strtotime($row->out))}}</span> </li>
			@endforeach
		</ul>
	</div>

</body>
</html>