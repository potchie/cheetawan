@extends('layouts.app')

@section('content')
	 <div class="panel-header panel-header-sm"></div>
	      <div class="content">
	        <div class="row">
				<div class="col-md-12">
		            <div class="card">
		              <div class="card-header">
		                <h4 class="card-title"><i class="now-ui-icons ui-1_calendar-60"></i> Personnel Schedule</h4>
		              </div>
		              <div class="card-body">
		              		<div class="col-md-12">
								<div>
									<div class="pull-right" style="float:right;">
											<a type="button" href="{{url('/admin/pSchedMany')}}" rel="tooltip" class="btn btn-info btn-round pull-right" data-original-title="" title="Add new Schedule">
						                            Add
						                            <i class="now-ui-icons ui-1_simple-add"></i>
			                				</a>
											<a data-toggle="modal" data-target="#selectmonth" type="button" rel="tooltip" class="btn btn-success btn-round pull-right" data-original-title="" title="Print Monthly Schedule">
						                            Print
						                            <i class="now-ui-icons ui-1_simple-add"></i>
			                				</a>
									</div>
									<br>
									<div style="float:right;">
										@include('calendar.fullcalendar')

									</div>
								</div>
							</div>
					  </div>
					</div>
				</div>
			</div>	
			@include('pschedule.selectmonth')		
	 </div>
@endsection

@section('extrajs')
	 <script src="{{asset('fullcalendar/moment.min.js')}}"></script>
	 <script src="{{asset('fullcalendar/fullcalendar.min.js')}}"></script>
	 {!! $calendar->script() !!}
@endsection