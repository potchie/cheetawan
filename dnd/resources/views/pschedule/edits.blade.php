@extends('layouts.app')
@section('content')
	 <div class="panel-header panel-header-sm"></div>
	      <div class="content">
	        <div class="row">
				<div class="col-md-12">
		            <div class="card">
		              <div class="card-header">
		                <h4 class="card-title"><i class="now-ui-icons ui-1_calendar-60"></i> Edit Schedule</h4>
		              </div>
		              <div class="card-body">
                            <h3>{{$sched->personnel->fname}} {{$sched->personnel->lname}}</h3>
                            <form id="schededit" method="post" onsubmit="return editSched({{$sched->personnel->id}})">
                            @csrf
                            <div class="instruction">
                            	  <div class="row">
                                    <input value="{{$sched->id}}" id ="id" name="id" hidden>
                                    <input value = "{{$sched->personnel->id}}" id='p_id' name ="p_id" hidden>
						              <div class="input-group no-border col-sm-6">
						              	<select id="shift" name="shift" onchange="getvalue(this)" class="selectpicker" required data-size="12" data-style="btn btn-round" title="shift">
						                          <option disabled selected>Select a Shift</option>
						                          @foreach($shift as $row)
						                          	<option value="{{$row->id}}" @if($sched->shift->id == $row->id) selected @endif>{{$row->name}}</option>                  	
						                          @endforeach
				                        </select>
						       
						              </div>
						              <div class="input-group no-border col-sm-6" >
						              	<input class="form-control" type="text" value="{{date('h:i a',strtotime($sched->shift->in))}} - {{date('h:i a',strtotime($sched->shift->out))}}" id="previewtime" name="previewtime" disabled>
				              	         <div class="input-group-append">
						                  <div class="input-group-text">
						                    <i class="now-ui-icons travel_info" rel="tooltip" title="Shift"></i>
						                  </div>
						                </div>
						              </div>
						           </div>
					              <div class="input-group no-border">
					                <input name="date" value="{{date('Y-m-d',strtotime($sched->date))}}" id="date" type="date" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Date"></i>
					                  </div>
					                </div>
					              </div>				          
                            </div>
                            <p></p>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button class="btn btn-info btn-round">Update <i class="now-ui-icons ui-1_send"></i></button>
                          </div>
                          </form>
                    </div>
                
		        </div>      	
			</div>
	 </div>
	 <script>
        function editSched(id)
        {
            var form_data = $("#schededit").serialize();
		       	$.ajax({
		         url : "{{url('/')}}"+"/admin/editSched",
		         data :  form_data,
		         type : "POST",
		        success : function(msg){
		        		console.log(msg);
			           
			            if(msg=="Count"){
                            error('Personnel has an existing schedule on the said date.');
			            }else{
			            	success('Schedule/s added successfully.')
			            	setTimeout(function(){window.location.href = "{{url('/')}}/admin/personnel/1";},1500);
			            }
		        	}
		       });
	 		return false;
        }

	 	function getvalue(id)
		{
			console.log(id.value);
			var form_data = {
					_token: $("input[name=_token]").val(),
					id:id.value,
				};
			  $.ajax({
				         url : "{{url('/')}}"+"/admin/getTime",
				         data :  form_data,
				         type : "POST",
				        success : function(msg){
				            //success();
				            console.log(msg);
		 					$('#previewtime').val(msg);
				        }
				       }); 
				           return false;
		}
	 </script>
@endsection

@section('extrajs')
	 
@endsection