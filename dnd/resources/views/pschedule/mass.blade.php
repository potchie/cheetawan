@extends('layouts.app')

@section('content')
	 <div class="panel-header panel-header-sm"></div>
	      <div class="content">
	        <div class="row">
				<div class="col-md-12">
		            <div class="card">
		              <div class="card-header">
		                <h4 class="card-title"><i class="now-ui-icons ui-1_calendar-60"></i> Batch Schedule</h4>
		              </div>
		              <div class="card-body">
		              	<form id="schedadd" method="post" onsubmit="return batchSched();">
		              		<input hidden id="_token" name="_token" value="{{ Session::token() }}">
		              		<div class="col-md-12">
								<div>
									<div class="row">
						              <div class="input-group no-border col-md-4 col-sm-12">
						              	<select id="shift" name="shift" onchange="getvalue(this)" class="selectpicker" required data-size="12" data-style="btn btn-round" title="shift">
						                          <option disabled selected>Select a Shift</option>
						                          @foreach($shift as $row)
						                          	<option value="{{$row->id}}">{{$row->name}}</option>                  	
						                          @endforeach
				                        </select>
						       
						              </div>
						              <div class="input-group no-border col-md-4 col-sm-12" >
						              	<input class="form-control" type="text" id="previewtime" name="previewtime" disabled>
				              	         <div class="input-group-append">
						                  <div class="input-group-text">
						                    <i class="now-ui-icons travel_info" rel="tooltip" title="Shift"></i>
						                  </div>
						                </div>
						              </div>
						              <div class="input-group no-border col-md-4 col-sm-12">
						                <input name="in" id="in" type="date" required class="form-control">
						                <div class="input-group-append">
						                  <div class="input-group-text">
						                    <i class="now-ui-icons travel_info" rel="tooltip" title="Start Date"></i>
						                  </div>
						                </div>
						              </div>
						           </div>
								</div>
							</div>
							<hr>
							Select all <input type="checkbox" onchange="selectall(this)" name="all" id="all">
							<div class="col-md-12">
								<div class="row">
									@foreach($personnel as $row)
											<div class="card col-md-3 active" id="">
												<div class="card-header">
													<input type="checkbox" onclick="deselectall(this)" name="personnel[]" value="{{$row->id}}">
									                <h4 class="card-title"><i class="now-ui-icons users_single-02"></i> {{$row->fname}} {{$row->mname[0]}}. {{$row->lname}} </h4>
								              	</div>
									            <div class="card-body">
													
												</div>
											</div>
									@endforeach
								</div>
							</div>
							<div class="">
								<button class="pull-right btn btn-round btn-primary">Save</button>
							</div>
						</form>
						@include('personnel/errorreport')
					  </div>
					</div>
				</div>
			</div>
	 </div>
	 <script>
	 	function batchSched()
	 	{
	 		var form_data = $("#schedadd").serialize();
	 		// alert($('input[name="personnel[]"]:checked').length);
	 		// console.log(form_data);
	 		if($('input[name="personnel[]"]:checked').length <= 0)
			{
				error('Please select at least one personnel.');
			}else{
				// var form_data={
		  //      		_token: $("input[name=_token]").val(),
		  //      		shift: $('#shift').val(),
		  //      		in: $('#in').val(),
		            
		  //           ajax: 1
		  //      };
		       	var form_data = $("#schedadd").serialize();
		       	$.ajax({
		         url : "{{url('/')}}"+"/admin/savebSched",
		         data :  form_data,
		         type : "POST",
		        success : function(msg){
		        		console.log(msg);
			            $res = msg.split('|');
			            $errs = $res[1].split('`');
			            $succ = $res[2].split('`');

			           // alert($errs.length);
			            if($errs.length > 1){
			            	$('#errorreport').modal('toggle');
			            	// Error
			            	for($i = 0; $i<$errs.length;$i++){
			            		if($errs[$i]!='')
			            			$("#errorlist").append('<div class="col-md-4"><li>'+ $errs[$i] +'</li></div>');
			            	}
			            	// Success
			            	for($i = 0; $i<$succ.length;$i++){
			            		if($succ[$i]!='')
			            			$("#savelist").append('<div class="col-md-4"><li>'+ $succ[$i] +'</li></div>');
			            	}

			            	$('#addSched').modal('toggle');
			            }else{
			            	success('Schedule/s added successfully.')
			            	setTimeout(function(){window.location.href = "{{url('/')}}/admin/Psched";},1500);
			            }
		        	}
		       });
		    }
	 		return false;
	 	}

	 	function selectall(item) {
	 		if(item.checked)
	 			$("input:checkbox").prop('checked', true);
	 		else
	 			$("input:checkbox").prop('checked', false);
	 	}

	 	function deselectall(item){
	 		if(!item.checked)
	 			$("#all").prop("checked",false);
	 	}

	 	function getvalue(id)
		{
			console.log(id.value);
			var form_data = {
					_token: $("input[name=_token]").val(),
					id:id.value,
				};
			  $.ajax({
				         url : "{{url('/')}}"+"/admin/getTime",
				         data :  form_data,
				         type : "POST",
				        success : function(msg){
				            //success();
				            console.log(msg);
		 					$('#previewtime').val(msg);
				        }
				       }); 
				           return false;
		}
	 </script>
@endsection

@section('extrajs')
	 
@endsection