               <div class="modal fade" id="addPSched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form id="schedadd" method="post" onsubmit="return addSched(1);">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Add new Schedule</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
                            	  <div class="row">
						              <div class="input-group no-border col-md-6 col-sm-6">
						              	<select id="shift" name="shift" onchange="getvalue(this)" class="selectpicker" required data-size="12" data-style="btn btn-round" title="shift">
						                          <option disabled selected>Select a Shift</option>
						                          @foreach($shift as $row)
						                          	<option value="{{$row->id}}">{{$row->name}}</option>                  	
						                          @endforeach
				                        </select>
						       
						              </div>
						              <div class="input-group no-border col-md-6 col-sm-6" >
						              	<input class="form-control" type="text" id="previewtime" name="previewtime" disabled>
				              	         <div class="input-group-append">
						                  <div class="input-group-text">
						                    <i class="now-ui-icons travel_info" rel="tooltip" title="Shift"></i>
						                  </div>
						                </div>
						              </div>
						           </div>
					              <div class="input-group no-border">
					                <input name="in" id="in" type="date" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Start Date"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					               <input name="out" id="out" type="date" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="End Date"></i>
					                  </div>
					                </div>
					              </div>					          
                            </div>
                            <p></p>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button class="btn btn-info btn-round" type="submit">Save <i class="now-ui-icons ui-1_send"></i></button>
                          </div>
                          </form>
                        </div>
                      </div>
             </div>