   		<div class="modal fade" id="selectmonth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" id="btnClose" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Select Month</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
                            	    <div class="input-group no-border col-md-6">
							                       <select id="month" name="month" class="selectpicker" required data-size="7" data-style="btn btn-round" title="month">
					                          
      					                          <option value="1" {{date('m') == 1 ? 'selected' : ''}}>January</option>
      					                          <option value="2" {{date('m') == 2 ? 'selected' : ''}}>February</option>
      					                          <option value="3" {{date('m') == 3 ? 'selected' : ''}}>March</option>
      					                          <option value="4" {{date('m') == 4 ? 'selected' : ''}}>April</option>
      					                          <option value="5" {{date('m') == 5 ? 'selected' : ''}}>May</option>
      					                          <option value="6" {{date('m') == 6 ? 'selected' : ''}}>June</option>
      					                          <option value="7" {{date('m') == 7 ? 'selected' : ''}}>July</option>
      					                          <option value="8" {{date('m') == 8 ? 'selected' : ''}}>August</option>
      					                          <option value="9" {{date('m') == 9 ? 'selected' : ''}}>September</option>
      					                          <option value="10" {{date('m') == 10 ? 'selected' : ''}}>October</option>
      					                          <option value="11" {{date('m') == 11 ? 'selected' : ''}}>November</option>
      					                          <option value="12" {{date('m') == 12 ? 'selected' : ''}} >December</option>

					                             </select>
							                     </div>
					                   </div> 
                              <p></p>
	                            <a class="btn btn-info btn-round pull-right"  style="float: right;"  onclick="selectmonth()">Select <i class="now-ui-icons ui-1_send"></i></a>
                              
                        </div>
                    </div>
              </div>
              <script>
              		function selectmonth(){
                    
                    $("#btnClose").click();
              			test = $('#month').val();
              			if(test >=1 )
              				window.open("{{url('admin/monthlysched/')}}/"+ test,'_blank');
              			else
              				error('Please select a month.');
              			
              		}
              </script>
        </div>