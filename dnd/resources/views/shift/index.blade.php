@extends('layouts.app')

@section('content')
 <div class="panel-header panel-header-sm"></div>
      <div class="content">
        <div class="row">
			<div class="col-md-12">
			            <div class="card">
			              <div class="card-header">
			                <h4 class="card-title"><i class="now-ui-icons users_single-02"></i> Shift</h4>
			              </div>
			              <div class="card-body">
			                <div class="table-responsive">
			                  <table id="datatable" class="table table-simple table-bordered" cellspacing="0" width="100%">
			                    <thead class=" text-primary">
			                      <tr align="center">
				                      <th>
				                        Name
				                      </th>
				                      <th>
				                        Alias
				                      </th>
				                      <th>
				                        In
				                      </th>
				                      <th>
				                        Out
				                      </th>
				                      <th>
				                        Status
				                      </th>
				                      <th>
				                      	Actions
				                      </th>
			                      </tr>
			                    </thead>
			                    <!-- <tfoot>
			                    	<tr class="text-center">
				                      <th>
				                        Name
				                      </th>
				                      <th>
				                        Age
				                      </th>
				                      <th>
				                        Gender
				                      </th>
				                      <th>
				                        Salary
				                      </th>
				                      <th>
				                      	Actions
				                      </th>
			                      	</tr>
			                    </tfoot> -->
			                    <tbody>
			                    	@if($personnel->count()>0)
				                    	@foreach($personnel as $row)
				                    		<tr class="text-center" >
						                        <td class="text-left">
						                           {{$row->name}}
						                        </td>
						                         <td class="text-left">
						                           {{strtoupper($row->alias)}}
						                        </td>
						                        <td>
						                          {{date('h:i a',strtotime($row->in))}}
						                        </td>
						                        <td>
						                          {{date('h:i a',strtotime($row->out))}}
						                        </td>
						                        <td>
						                        	@if($row->is_active ==1)
						                        		<span class="text-success">Active</span>
						                        	@else
						                        		<span class="text-danger">Inactive</span>
						                        	@endif
						                        </td>
						                        <td>
						                            <button @if($row->is_active != 1) hidden @endif type="button" rel="tooltip" title="Edit" data-toggle="modal" data-target="#editModal" class="btn btn-info btn-icon btn-sm " onclick="placeValue( {{$row->id }} ,'{{$row->name}}','{{$row->alias}}','{{$row->in}}','{{$row->out}}' )">
						                            <i class="now-ui-icons ui-2_settings-90"></i>
						                          </button>
						                           @if($row->is_active == 1)
							                          <button type="button" rel="tooltip" title="Deactivate" onclick="deact({{$row->id}},1)" class="btn btn-danger btn-icon btn-sm ">
							                            <i class="now-ui-icons ui-1_simple-remove"></i>
							                          </button>
							                       @else
								                       <button type="button" rel="tooltip" title="Activate" onclick="deact({{$row->id}},2)" class="btn btn-success btn-icon btn-sm ">
								                            <i class="now-ui-icons ui-1_check"></i>
							                          </button>
							                       		
							                       @endif
						                        </td>
					                      	</tr>
				                    	@endforeach
				                    @else
				                    	<tr>
				                    		<td align="center" colspan="6">
				                    			<span class="text-primary">No data found</span>
				                    		</td>
				                    	</tr>
				                    @endif
			                    </tbody>
			                  </table>
			                   <button type="button" data-toggle="modal" data-target="#addModal" rel="tooltip" class="btn btn-info btn-round pull-right" data-original-title="" title="Add new Shift">
				                            Add
				                            <i class="now-ui-icons ui-1_simple-add"></i>
				                </button>
			                </div>
			              </div>
			            </div>
			  </div>
             @include('shift/edit')
             @include('shift/add')
		</div>
	</div>
	 <script type="text/javascript">
	 			function deact(id,type)
	 			{
	 				if(type == 1)
	 					text = "deactivate";
	 				else
	 					text = "activate"
	 				swal({
			          title: 'Confirm',
			          text: "Are you sure you want to "+ text +" this shift?",
			          type: 'warning',
			          showCancelButton: true,
			          confirmButtonColor: '#3085d6',
			          cancelButtonColor: '#d33',
			          confirmButtonText: text.toUpperCase(),
			        }).then((result) => {
						if(result){
							var form_data = {
								_token: $("input[name=_token]").val(),
								id:id,
								type:type
							};
							$.ajax({
						         url : "{{url('/')}}"+"/admin/deact_shift",
						         data :  form_data,
						         type : "POST",
						        success : function(msg){
						            //success();
						            //console.log(msg);
						           
						            if(msg=="Error"){
						                error("The system encountered an error. Please contact the administrator.");
						            }

						            if(msg=="Success"){
						            	success("Shift "+ text +"d successfully.");
						            	setTimeout(function(){window.location.reload();},1500);
						            }

						        }
						       }); 
						}
						
			        })
	 			}

	 			function placeValue(id,name,alias,t_in,t_out)
	 			{

	 				$('#eid').val(id);
	 				$('#ename').val(name);
	 				$('#ealias').val(alias);
	 				$('#ein').val(t_in);
	 				$('#eout').val(t_out);
	 			}
    //document.getElementById("beg_date").onchange = function() {myFunction()};
			    function add_Shift(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		name: $('#name').val(),
			       		alias: $('#alias').val(),
			       		in: $('#in').val(),
			            out: $('#out').val(),
			            ajax: 1
			       };
			        
			       $.ajax({
			         url : "{{route('shift.store')}}",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            //console.log(msg);
			            var res = msg.split('|');
			            if(res[0]=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }
			            if(res[0]=="Success"){
			            	success('Shift added successfully.')
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			           return false;
			       }

			       function edit_Shift(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		id: $('#eid').val(),
			       		ename: $('#ename').val(),
			       		ealias:$('#ealias').val(),
			       		ein: $('#ein').val(),
			            eout: $('#eout').val(),
			            ajax: 1
			       };
			        
			       $.ajax({
			        url : "{{url('/')}}"+"/admin/updateShift",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            console.log(msg);
			            if(msg=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }
			            if(msg=="Success"){
			            	success('Shift updated recorded.')
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			           return false;
			       }

  </script>	
@endsection

@section('extrajs')
 <script>
    $(document).ready(function() {
      $('#datatable').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });


      func.initDateTimePicker();
      if ($('.slider').length != 0) {
        func.initSliders();
      }
    });
</script>
@endsection