   		<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form method="post" onsubmit="return edit_Shift()">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Edit Shift</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
                            	  <input id="eid" name="eid" hidden>
					              <div class="input-group no-border">
					                <input name="ename" id="ename" type="text" value="" required class="form-control" placeholder="Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Name"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="ealias" id="ealias" minlength="2" maxlength="2" type="text" value="" required class="form-control" placeholder="Alias">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Alias"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="ein" id="ein" type="time" value="" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Time In"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="eout" id="eout" type="time" value="" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Time Out"></i>
					                  </div>
					                </div>
					              </div>
					            </div> 
                            <p></p>
                        </div>
	                          <div class="modal-footer pull-right">
	                            <button class="btn btn-info btn-round" type="submit">Update <i class="now-ui-icons ui-1_send"></i></button>
	                          </div>
                          </form>
                    </div>
              </div>
        </div>