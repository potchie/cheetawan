               <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                      		<form method="post" onsubmit="return add_Shift()">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Add new Shift</h5>
                          </div>
                          <div class="modal-body">
                            	<div class="instruction">
					              <div class="input-group no-border">
					                <input name="name" id="name" type="text" value="" required class="form-control" placeholder="Name / Alias">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Name"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="alias" id="alias" minlength="2" maxlength="2" type="text" value="" required class="form-control" placeholder="Alias">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Alias"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="in" id="in" type="time" value="" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Time In"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="out" id="out" type="time" value="" required class="form-control" >
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Time Out"></i>
					                  </div>
					                </div>
					              </div>
				              	</div>
	                            <p></p>
	                          </div>
	                          <div class="modal-footer justify-content-center">
	                            <button class="btn btn-info btn-round" type="submit">Save <i class="now-ui-icons ui-1_send"></i></button>
	                          </div>
                          </form>
                        </div>
                    </div>
             </div>

