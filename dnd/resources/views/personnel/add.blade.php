               <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form method="post" onsubmit="return add_Personnel();">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Add new Personnel</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
					              <div class="input-group no-border">
					                <input name="fname" id="fname" type="text" value="" required class="form-control" placeholder="First Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Given Name"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="mname" id="mname" type="text" value="" required class="form-control" placeholder="Middle Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="lname" id="lname" type="text" value="" required class="form-control" placeholder="Last Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="row">
							              <div class="input-group no-border col-md-6">
							                <input name="age" id="age" type="number" value="" required class="form-control" placeholder="Age">
							                <div class="input-group-append">
							                  <div class="input-group-text">
							                    <i class="now-ui-icons ui-1_calendar-60"></i>
							                  </div>
							                </div>
							              </div>
							              <div class="input-group no-border col-md-6">
							               <select id="gender" name="gender" class="form-control" required data-size="7" data-style="btn btn-round" title="gender">
					                          <option disabled selected>Gender</option>
					                          <option value="Male">Male</option>
					                          <option value="Female">Female</option>
					                        </select>
							              </div>
					              </div>
					              <div class="input-group no-border">
							                <input name="salary" id="salary" type="number" required value="" class="form-control" placeholder="Salary">
							                <div class="input-group-append">
							                  <div class="input-group-text">
							                    <i class="now-ui-icons business_money-coins"></i>
							                  </div>
							                </div>
					              </div>
					          
                            </div>
                            <p></p>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button class="btn btn-info btn-round" type="submit">Save <i class="now-ui-icons ui-1_send"></i></button>
                          </div>
                          </form>
                        </div>
                      </div>
             </div>