@extends('layouts.app')
@section('content')
	 <div class="panel-header panel-header-sm"></div>
	      <div class="content">
	        <div class="row">
				<div class="col-md-12">
		            <div class="card">
		              <div class="card-header">
		                <h4 class="card-title"><i class="now-ui-icons users_single-02"></i> Profile</h4>
		              </div>
		              <div class="card-body">
		              		<div class="row">
			              		<div class="col-md-3">
						            <div class="card card-user">
						              <div class="image">
						                <img src="{{asset('/img/bg5.jpg')}}" alt="...">
						              </div>
						              <div class="card-body">
						                <div class="author">
						                  <!-- <a href="#"> -->
						                    <img class="avatar border-gray" src="{{asset('/img/mike.png')}}" alt="...">
						                    <h5 class="title text-primary">{{$personnel->fname}} {{$personnel->mname[0]}}. {{$personnel->lname}}</h5>
						                    <span style="font-size:xx-small; color: red;" @if(Auth::user()->role->restriction != '1') hidden @endif>
						                    	<a data-toggle="modal" onclick="filldata({{$personnel->id}},'{{ $personnel->fname }}', '{{$personnel->mname}}','{{$personnel->lname}}',{{$personnel->age}},'{{$personnel->gender}}',{{$personnel->salary}})" href="" data-target="#editProfile" >
							                        Edit Profile
						                    	</a>
						                    	|
												<a data-toggle="modal" onclick="fillPass({{$personnel->id}})" href="" data-target="#changePass" >
						                    		Change Password
						                    	</a>
						                	</span>
						                  <!-- </a> -->
						                 <!--  <p class="description">
						                    michael24
						                  </p> -->
						                  		@include('personnel.editprofile')
												@include('personnel.changepw')
						                </div>
						               <!--  <p class="description text-center">
						                  "Lamborghini Mercy
						                  <br> Your chick she so thirsty
						                  <br> I'm in that two seat Lambo"
						                </p> -->
						              </div>
						              <hr>
						              <!-- <div class="button-container">
						                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
						                  <i class="fab fa-facebook-square"></i>
						                </button>
						                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
						                  <i class="fab fa-twitter"></i>
						                </button>
						                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
						                  <i class="fab fa-google-plus-square"></i>
						                </button>
						              </div> -->
						            </div>
						          </div>
			              		<div class="col-md-9">
									<div>
										@include('calendar.fullcalendar')

										<div class="pull-right">
												<button type="button" data-toggle="modal" data-target="#addSched" rel="tooltip" class="btn btn-info btn-round pull-right" data-original-title="" title="Add new Schedule">
							                            Add
							                            <i class="now-ui-icons ui-1_simple-add"></i>
				                				</button>
										</div>
									</div>
								</div>
							</div>
							@include('personnel.sched')
							@include('personnel.errorreport')
						
					  </div>
					</div>
				</div>
			</div>
	 </div>
	 <script>
		 function change_Pass()
		{
			if($('#password').val() == $('#rpassword').val()){
				var form_data={
					_token: $("input[name=_token]").val(),
					id: $('#cid').val(),
					password: $('#password').val(),
					ajax: 1
				};  
				$.ajax({
				url : "{{url('/')}}"+"/admin/changePass",
					data :  form_data,
					type : "POST",
				success : function(msg){
					//success();
					console.log(msg);
					if(msg=="Error"){
						error("The system encountered an error. Please contact the administrator.");
					}
					if(msg=="Success"){
						success('Password updated.')
						setTimeout(function(){window.location.reload();},1500);
					}
				}
				}); 
			}else{
				error("Password did not match");
			}
			return false;

		}

		function filldata(id,fname,mname,lname,age,gender,salary)
		{
			$('#eid').val(id);
			$('#fname').val(fname);
			$('#mname').val(mname);
			$('#lname').val(lname);
			$('#age').val(age);
			$('#gender').val(gender);
			$('#salary').val(salary);
		}

		function fillPass(id)
		{
			$('#cid').val(id);
		}

		function edit_Personnel(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		id: $('#eid').val(),
			       		fname: $('#fname').val(),
			       		mname: $('#mname').val(),
			            lname: $('#lname').val(),
			            age: $('#age').val(),
			            gender: $('#gender').val(),
			            salary: $('#salary').val(),
			            ajax: 1
			       };
			        
			       $.ajax({
			        url : "{{url('/')}}"+"/admin/updatePersonnel",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            console.log(msg);
			            if(msg=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }
			            if(msg=="Success"){
			            	success('Personnel updated.')
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			           return false;
		}
	</script>

@endsection
@section('extrajs')
	<script src="{{asset('fullcalendar/moment.min.js')}}"></script>
	 <script src="{{asset('fullcalendar/fullcalendar.min.js')}}"></script>
	 {!! $calendar->script() !!}
@endsection