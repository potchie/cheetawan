   		<div class="modal fade" id="errorreport" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form method="post" onsubmit="return edit_Personnel();">
                          <div class="modal-header">
                           <!--  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button> -->
                            <h5 class="modal-title text-primary" id="myModalLabel"><i class="now-ui-icons travel_info"></i> Summary Reports</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
                            	<h6 class="text-success">Saved</h6>
                            		<span style="font-size: x-small" class="text-success">Date of Schedules successfully saved.</span>
                            		<ul>
                            			<div id="savelist" class="row">
                            			</div>
                            		</ul>
                            	<h6 class="text-danger">Unsaved</h6>
                            		<span style="font-size: x-small" class="text-danger">The following dates are not saved, they were currently existing.</span>
                            		<ul>
                            			<div class="row" id="errorlist">
                            				
                            			</div>
                            		</ul>
                          	</div>
                          <div class="modal-footer pull-right">
                            <a class="btn btn-info btn-round" onclick="history.go(0)"><i class="now-ui-icons media-1_button-play"></i><span style="color:white;"> Proceed</span></a>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>