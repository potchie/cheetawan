   <div class="modal fade" id="addSched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form id="schedadd" method="post" onsubmit="return addSched({{$personnel->id}});">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Add new Schedule</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
                            	  <div class="row">
						              <div class="input-group no-border col-md-6 col-sm-6">
						              	<select id="shift" name="shift" onchange="getvalue(this)" class="selectpicker" required data-size="12" data-style="btn btn-round" title="shift">
						                          <option disabled selected>Select a Shift</option>
						                          @foreach($shift as $row)
						                          	<option value="{{$row->id}}">{{$row->name}}</option>                  	
						                          @endforeach
				                        </select>
						       
						              </div>
						              <div class="input-group no-border col-md-6 col-sm-6">
						              	<input class="form-control" type="text" id="previewtime" name="previewtime" disabled>
				              	         <div class="input-group-append">
						                  <div class="input-group-text">
						                    <i class="now-ui-icons travel_info" rel="tooltip" title="Shift"></i>
						                  </div>
						                </div>
						              </div>
						           </div>
					              <div class="input-group no-border">
					                <input name="in" id="in" type="date" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Start Date"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					               <input name="out" id="out" type="date" required class="form-control">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="End Date"></i>
					                  </div>
					                </div>
					              </div>					          
                            </div>
                            <p></p>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button class="btn btn-info btn-round" type="submit">Save <i class="now-ui-icons ui-1_send"></i></button>
                          </div>
                          </form>
                        </div>
                      </div>
             </div>


<script>
	function test() {

		alert(success);
	}

	function addSched(ids)
	{
		var date1 = $('#in').val().split('-');
		var date2 = $('#out').val().split('-');

		if($('#in').val() > $('#out').val())
		{
			error('Start Date cannot be greater than the End Date.');
			return false;
		}else if( date1[1] != date2[1] || date1[0] != date2[0]){
			error('Please select two dates within the same month only.');
			return false;
		}else{
			var form_data={
	       		_token: $("input[name=_token]").val(),
	       		shift: $('#shift').val(),
	       		in: $('#in').val(),
	            out: $('#out').val(),
	            id: ids,
	            ajax: 1
	       };
	        
	       $.ajax({
	         url : "{{url('/')}}"+"/admin/saveSched",
	         data :  form_data,
	         type : "POST",
	        success : function(msg){
		            //success();
		            // console.log(msg);
		            // var res = msg.split('|');
		            $res = msg.split('|');
		            $errs = $res[1].split('`');
		            $succ = $res[2].split('`');
		            if($errs.length > 0){
		            	$('#errorreport').modal('toggle');
		            	// Error
		            	for($i = 0; $i<$errs.length;$i++){
		            		if($errs[$i]!='')
		            			$("#errorlist").append('<div class="col-md-4"><li>'+ $errs[$i] +'</li></div>');
		            	}

		            	// Success
		            	for($i = 0; $i<$succ.length;$i++){
		            		if($succ[$i]!='')
		            			$("#savelist").append('<div class="col-md-4"><li>'+ $succ[$i] +'</li></div>');
		            	}

		            	$('#addSched').modal('toggle');
		            }else{
		            	success('Schedule/s added successfully.')
		            	setTimeout(function(){window.location.reload();},1500);
		            }
	        	}
	       }); 
	       return false;   
		}

	}

	function getvalue(id)
	{
		console.log(id.value);
		var form_data = {
				_token: $("input[name=_token]").val(),
				id:id.value,
			};
		  $.ajax({
			         url : "{{url('/')}}"+"/admin/getTime",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            console.log(msg);
	 					$('#previewtime').val(msg);
			        }
			       }); 
			           return false;
	}

</script>

