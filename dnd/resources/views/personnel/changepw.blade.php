<div class="modal fade" id="changePass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-notice">
    <div class="modal-content">
      <form method="post" onsubmit="return change_Pass();">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <h5 class="modal-title" id="myModalLabel">Change Password</h5>
          </div>
          <div class="modal-body">
            <div class="instruction">
	              <div class="input-group no-border">
				  	<input id="cid" name="cid" hidden>
	                <input name="password" id="password" type="password" value="" required class="form-control" placeholder="Password">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Password"></i>
	                  </div>
	                </div>
	              </div>
                  <div class="input-group no-border">
	                <input name="rpassword" id="rpassword" type="password" value="" required class="form-control" placeholder="Password">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Password"></i>
	                  </div>
	                </div>
	              </div>
            </div>
            <p></p>
          </div>
          <div class="modal-footer justify-content-center">
            <button class="btn btn-info btn-round" type="submit">Submit <i class="now-ui-icons ui-1_send"></i></button>
          </div>
      </form>
    </div>
  </div>
</div>