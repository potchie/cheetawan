   		<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form method="post" onsubmit="return edit_Personnel();">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Edit Personnel</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
                            		<input id="eid" name="eid" hidden>
					              <div class="input-group no-border">
					                <input name="efname" id="efname" type="text" value="" required class="form-control" placeholder="First Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="Given Name"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="emname" id="emname" type="text" value="" required class="form-control" placeholder="Middle Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="elname" id="elname" type="text" value="" required class="form-control" placeholder="Last Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="row">
							              <div class="input-group no-border col-md-6">
							                <input name="eage" id="eage" type="number" value="" required class="form-control" placeholder="Age">
							                <div class="input-group-append">
							                  <div class="input-group-text">
							                    <i class="now-ui-icons ui-1_calendar-60"></i>
							                  </div>
							                </div>
							              </div>
							              <div class="input-group no-border col-md-6">
							               <select id="egender" name="egender" class="form-control" required data-size="7" data-style="btn btn-round" title="gender">
					                          <option disabled>Gender</option>
					                          <option value="Male" selected>Male</option>
					                          <option value="Female">Female</option>
					                        </select>
							              </div>
					              </div>
					              <div class="input-group no-border">
							                <input name="esalary" id="esalary" type="number" required value="" class="form-control" placeholder="Salary">
							                <div class="input-group-append">
							                  <div class="input-group-text">
							                    <i class="now-ui-icons business_money-coins"></i>
							                  </div>
							                </div>
					              </div>
					          
                            </div>
                            <p></p>
                          </div>
                          <div class="modal-footer pull-right">
                            <button class="btn btn-info btn-round" type="submit">Update <i class="now-ui-icons ui-1_send"></i></button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>