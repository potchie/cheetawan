@extends('layouts.app')

@section('content')
 <div class="panel-header panel-header-sm"></div>
      <div class="content">
        <div class="row">
			<div class="col-md-12">
			            <div class="card">
			              <div class="card-header">
			                <h4 class="card-title"><i class="now-ui-icons users_single-02"></i> Personnel</h4>
			              </div>
			              <div class="card-body">
			                <div class="table-responsive">
			                  <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
			                    <thead class=" text-primary">
			                      <tr align="center">
				                      <th>
				                        Name
				                      </th>
				                      <th>
				                        Age
				                      </th>
				                      <th>
				                        Gender
				                      </th>
				                      <th>
				                        Salary
				                      </th>
				                      <th>
				                        Status
				                      </th>
				                      <th>
				                      	Actions
				                      </th>
			                      </tr>
			                    </thead>
			                    <tbody>
			                    	@if($personnel->count()>0)
				                    	@foreach($personnel as $row)
				                    		<tr class="text-center">
						                        <td class="text-left">
						                           {{$row->fname}} {{$row->mname[0]}}. {{$row->lname}}
						                        </td>
						                        <td>
						                          {{$row->age}}
						                        </td>
						                        <td>
						                          {{$row->gender}}
						                        </td>
						                        <td class="text-right">
						                          {{$row->salary}}
						                        </td>
						                        <td>
						                        	@if($row->status ==1)
						                        		<span class="text-success">Active</span>
						                        	@else
						                        		<span class="text-danger">Inactive</span>
						                        	@endif
						                        </td>
						                        <td>
						                          
						                          <a @if($row->status != 1) hidden @endif type="button" rel="tooltip" title="Profile" class="btn btn-info btn-icon btn-sm" href="{{url('/admin/personnel',[$row->id])}}">
						                            <i class="now-ui-icons users_single-02"></i>
						                          </a>
						                          <button  @if($row->status != 1) hidden @endif type="button" rel="tooltip" title="Edit" data-toggle="modal" data-target="#editModal" class="btn btn-tumblr btn-icon btn-sm " 
						                          		onclick="placeValue( {{$row->id}} ,'{{$row->fname}}','{{$row->mname}}','{{$row->lname}}',{{$row->age}},'{{$row->gender}}', {{$row->salary}} ) "
						                            >
						                            <i class="now-ui-icons ui-2_settings-90"></i>
						                          </button>
						                          @if($row->status == 1)
							                          <button type="button" rel="tooltip" title="Deactivate" onclick="deact({{$row->id}},1)" class="btn btn-danger btn-icon btn-sm ">
							                            <i class="now-ui-icons ui-1_simple-remove"></i>
							                          </button>
							                       @else
								                       <button type="button" rel="tooltip" title="Activate" onclick="deact({{$row->id}},2)" class="btn btn-success btn-icon btn-sm ">
								                            <i class="now-ui-icons ui-1_check"></i>
							                          </button>
							                       		
							                       @endif
						                        </td>
					                      	</tr>
				                    	@endforeach
				                    @else
				                    	<tr>
				                    		<td align="center" colspan="6">
				                    			<span class="text-primary">No data found</span>
				                    		</td>
				                    	</tr>
				                    @endif
			                    </tbody>
			                  </table>
			                   <button type="button" data-toggle="modal" data-target="#addModal" rel="tooltip" class="btn btn-info btn-round pull-right" data-original-title="" title="Add new Employee">
				                            Add
				                            <i class="now-ui-icons ui-1_simple-add"></i>
				                </button>
			                </div>
			              </div>
			            </div>
			  </div>
             @include('personnel/add')
             @include('personnel/edit')
		</div>
	</div>
	 <script type="text/javascript">
	 			function deact(id,type)
	 			{
	 				if(type == 1)
	 					text = "deactivate";
	 				else
	 					text = "activate"
	 				swal({
			          title: 'Confirm',
			          text: "Are you sure you want to "+ text +" this personnel?",
			          type: 'warning',
			          showCancelButton: true,
			          confirmButtonColor: '#3085d6',
			          cancelButtonColor: '#d33',
			          confirmButtonText: text.toUpperCase(),
			        }).then((result) => {
						if(result){
							var form_data = {
								_token: $("input[name=_token]").val(),
								id:id,
								type:type
							};
							$.ajax({
						         url : "{{url('/')}}"+"/admin/deact",
						         data :  form_data,
						         type : "POST",
						        success : function(msg){
						            //success();
						            //console.log(msg);
						           
						            if(msg=="Error"){
						                error("The system encountered an error. Please contact the administrator.");
						            }

						            if(msg=="Success"){
						            	success("Personnel "+ text +"d successfully.");
						            	setTimeout(function(){window.location.reload();},1500);
						            }

						        }
						       }); 
						}
						
			        })
	 			}

	 			function placeValue(id,fname,mname,lname,age,gender,salary)
	 			{

	 				$('#eid').val(id);
	 				$('#efname').val(fname);
	 				$('#emname').val(mname);
	 				$('#elname').val(lname);
	 				$('#eage').val(age);
	 				$('#egender').val(gender);
	 				$('#esalary').val(salary);

	 			}
    //document.getElementById("beg_date").onchange = function() {myFunction()};
			    function add_Personnel(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		fname: $('#fname').val(),
			       		mname: $('#mname').val(),
			            lname: $('#lname').val(),
			            age: $('#age').val(),
			            gender: $('#gender').val(),
			            salary: $('#salary').val(),
			            ajax: 1
			       };
			       
			       if($('#gender').val()){
						       $.ajax({
						         url : "{{route('personnel.store')}}",
						         data :  form_data,
						         type : "POST",
						        success : function(msg){
						            //success();
						            //console.log(msg);
						            var res = msg.split('|');
						            if(res[0]=="Error"){
						                error("The system encountered an error. Please contact the administrator.");
						            }
						            if(res[0]=="Success"){
						            	success('Personnel added successfully.')
						            	setTimeout(function(){window.location.reload();},1500);
						            }

						        }
						       });
					}else{
						error('Please select a gender.');
					}

			           return false;
			       }

			       function edit_Personnel(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		id: $('#eid').val(),
			       		fname: $('#efname').val(),
			       		mname: $('#emname').val(),
			            lname: $('#elname').val(),
			            age: $('#eage').val(),
			            gender: $('#egender').val(),
			            salary: $('#esalary').val(),
			            ajax: 1
			       };
			        
			       $.ajax({
			        url : "{{url('/')}}"+"/admin/updatePersonnel",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            console.log(msg);
			            if(msg=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }
			            if(msg=="Success"){
			            	success('Personnel updated.')
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			           return false;
			       }

  </script>	
@endsection

@section('extrajs')
 <script>
    $(document).ready(function() {
      $('#datatable').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
</script>
@endsection