@extends('layouts.app')
@section('content')
	 <div class="panel-header panel-header-sm"></div>
	      <div class="content">
	        <div class="row">
				<div class="col-md-12">
		            <div class="card">
		              <div class="card-header">
		                <h4 class="card-title"><i class="now-ui-icons users_single-02"></i> Profile</h4>
		              </div>
		              <div class="card-body">
		              		<div class="row">
			              		<div class="col-md-3">
						            <div class="card card-user">
						              <div class="image">
						                <img src="{{asset('/img/bg5.jpg')}}" alt="...">
						              </div>
						              <div class="card-body">
						                <div class="author">
						                  <!-- <a href="#"> -->
						                    <img class="avatar border-gray" src="{{asset('/img/mike.png')}}" alt="...">
						                    <h5 class="title text-primary">
												
											</h5>
						                    <span style="font-size:xx-small; color: red;">
												<a data-toggle="modal" onclick="fillPass({{$personnel->id}})" href="" data-target="#changePass" >
						                    		Change Password
						                    	</a>
						                	</span>
						                  <!-- </a> -->
						                 <!--  <p class="description">
						                    michael24
						                  </p> -->
												@include('personnel.changepw')
						                </div>
						               <!--  <p class="description text-center">
						                  "Lamborghini Mercy
						                  <br> Your chick she so thirsty
						                  <br> I'm in that two seat Lambo"
						                </p> -->
						              </div>
						              <hr>
						              <!-- <div class="button-container">
						                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
						                  <i class="fab fa-facebook-square"></i>
						                </button>
						                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
						                  <i class="fab fa-twitter"></i>
						                </button>
						                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
						                  <i class="fab fa-google-plus-square"></i>
						                </button>
						              </div> -->
						            </div>
						          </div>
			              		<div class="col-md-9">
									<div>
										<form method="post" onsubmit="return edit_Personnel();">
											<div class="row">
												<div class="col-sm-2">
													<label>Username</label>
												</div>
												<div class="input-group no-border col-sm-10">
														<input name="Username" id="username" disabled value="{{$personnel->username}}" required value="{{$personnel->salary}}" class="form-control" placeholder="Salary">
														<div class="input-group-append">
														<div class="input-group-text">
															<i class="now-ui-icons business_money-coins"></i>
														</div>
														</div>
												</div>
											</div>
											<div class="instruction">
												<div class="row">
													<div class="col-sm-2">
														<label>First name</label>
													</div>
													<div class="input-group no-border col-sm-10">
														<input id="eid" name="eid" value="{{$personnel->id}}" hidden>
														<input name="fname" id="fname" type="text" value="{{$personnel->fname}}" required class="form-control" placeholder="First Name">
														<div class="input-group-append">
														<div class="input-group-text">
															<i class="now-ui-icons travel_info" rel="tooltip" title="Given Name"></i>
														</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-2">
														<label>Middle name</label>
													</div>
													<div class="input-group no-border col-sm-10">
														<input name="mname" id="mname" type="text" value="{{$personnel->mname}}" required class="form-control" placeholder="Middle Name">
														<div class="input-group-append">
														<div class="input-group-text">
															<i class="now-ui-icons travel_info"></i>
														</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-2">
														<label>Last name</label>
													</div>
													<div class="input-group no-border col-sm-10">
														<input name="lname" id="lname" type="text" value="{{$personnel->lname}}" required class="form-control" placeholder="Last Name">
														<div class="input-group-append">
														<div class="input-group-text">
															<i class="now-ui-icons travel_info"></i>
														</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-2">
														<label>Nickname</label>
													</div>
													<div class="input-group no-border col-sm-10">
														<input name="name" id="name" type="text" value="{{$personnel->name}}" required class="form-control" placeholder="Nickname">
														<div class="input-group-append">
														<div class="input-group-text">
															<i class="now-ui-icons travel_info" rel="tooltip" title="Given Name"></i>
														</div>
														</div>
													</div>
												</div>									
											</div>
											<p></p>
										</div>
										<div class="modal-footer justify-content-center">
											<button class="btn btn-info btn-round" type="submit">Update <i class="now-ui-icons ui-1_send"></i></button>
										</div>
									</form>
								</div>
							</div>
							@include('personnel.sched')
							@include('personnel.errorreport')
						
					  </div>
					</div>
				</div>
			</div>
	 </div>
	 <script>
		function change_Pass()
		{
			if($('#password').val() == $('#rpassword').val()){
				var form_data={
					_token: $("input[name=_token]").val(),
					id: $('#cid').val(),
					password: $('#password').val(),
					ajax: 1
				};  
				$.ajax({
				url : "{{url('/')}}"+"/admin/changePass",
					data :  form_data,
					type : "POST",
				success : function(msg){
					//success();
					console.log(msg);
					if(msg=="Error"){
						error("The system encountered an error. Please contact the administrator.");
					}
					if(msg=="Success"){
						success('Password updated.')
						setTimeout(function(){window.location.reload();},1500);
					}
				}
				}); 
			}else{
				error("Password did not match");
			}
			return false;

		}

		function filldata(id,fname,mname,lname,age,gender,salary)
		{
			$('#eid').val(id);
			$('#fname').val(fname);
			$('#mname').val(mname);
			$('#lname').val(lname);
			$('#age').val(age);
			$('#gender').val(gender);
			$('#salary').val(salary);
		}

		function fillPass(id)
		{
			$('#cid').val(id);
		}

		function edit_Personnel(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		id: $('#eid').val(),
			       		fname: $('#fname').val(),
			       		mname: $('#mname').val(),
						lname: $('#lname').val(),
						name: $('#name').val(),
			            ajax: 1
			       };
			        
			       $.ajax({
			        url : "{{url('/')}}"+"/admin/updateUser",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            console.log(msg);
			            if(msg=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }
			            if(msg=="Success"){
			            	success('Profile updated.')
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			           return false;
		}
	</script>

@endsection
