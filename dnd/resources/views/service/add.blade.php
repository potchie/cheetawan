<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notice">
                        <div class="modal-content">
                          <form id="addService" method="post" onsubmit="return addService();">
                          	<input hidden id="_token" name="_token" value="{{ Session::token() }}">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Add new Service</h5>
                          </div>
                          <div class="modal-body">
                            <div class="instruction">
					              <div class="input-group no-border">
					                <input name="name" id="name" type="text" value="" required class="form-control" placeholder="Service Name">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="ie. Swidish, Foot Massage, etc."></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="duration" id="duration" type="number" value="" required class="form-control" placeholder="Duration in Minutes">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons travel_info" rel="tooltip" title="duration in minutes"></i>
					                  </div>
					                </div>
					              </div>
					              <div class="input-group no-border">
					                <input name="price" id="price" type="price" value="" required class="form-control" placeholder="Price">
					                <div class="input-group-append">
					                  <div class="input-group-text">
					                    <i class="now-ui-icons business_money-coins"></i>
					                  </div>
					                </div>
					              </div>
                            </div>
                            <p></p>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button class="btn btn-info btn-round" type="submit">Save <i class="now-ui-icons ui-1_send"></i></button>
                          </div>
                          </form>
                        </div>
                      </div>
             </div>