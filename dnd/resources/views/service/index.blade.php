@extends('layouts.app')
@section('content')
<div class="panel-header panel-header-sm"></div>
      <div class="content">
        <div class="row">
			<div class="col-md-12">
				<div class="card">
			              <div class="card-header">
			                <h4 class="card-title"><i class="now-ui-icons shopping_box"></i> Services</h4>
			              </div>
			              <div class="card-body">
			              					                <div class="table-responsive">
			                  <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
			                    <thead class=" text-primary">
			                      <tr align="center">
				                      <th>
				                        Name of Service
				                      </th>
				                      <th>
				                        Duration (minutes)
				                      </th>
				                      <th>
				                        Price
				                      </th>
				                      <th>
				                        Status
				                      </th>
				                      <th>
				                      	Actions
				                      </th>
			                      </tr>
			                    </thead>
			                    <tbody>
			                    	@if($services->count()>0)
				                    	@foreach($services as $row)
				                    		<tr class="text-center">
						                        <td class="text-left">
						                           {{$row->name}}
						                        </td>
						                        <td>
						                          {{$row->duration}}
						                        </td>
						                        <td>
						                          {{$row->price}}
						                        </td>
						                        <td>
						                        	@if($row->status ==1)
						                        		<span class="text-success">Active</span>
						                        	@else
						                        		<span class="text-danger">Inactive</span>
						                        	@endif
						                        </td>
						                        <td>
						                          <button  @if($row->status != 1) hidden @endif type="button" rel="tooltip" title="Edit" data-toggle="modal" data-target="#editServie" class="btn btn-tumblr btn-icon btn-sm " 
						                          		onclick="placeValue( {{$row->id}} ,'{{$row->name}}',{{$row->duration}},{{$row->price}})"
						                            >
						                            <i class="now-ui-icons ui-2_settings-90"></i>
						                          </button>
						                          @if($row->status == 1)
							                          <button type="button" rel="tooltip" title="Deactivate" onclick="deact({{$row->id}},1)" class="btn btn-danger btn-icon btn-sm ">
							                            <i class="now-ui-icons ui-1_simple-remove"></i>
							                          </button>
							                       @else
								                       <button type="button" rel="tooltip" title="Activate" onclick="deact({{$row->id}},2)" class="btn btn-success btn-icon btn-sm ">
								                            <i class="now-ui-icons ui-1_check"></i>
							                          </button>
							                       		
							                       @endif
						                        </td>
					                      	</tr>
				                    	@endforeach
				                    @else
				                    	<tr>
				                    		<td align="center" colspan="5">
				                    			<span class="text-primary">No data found</span>
				                    		</td>
				                    	</tr>
				                    @endif
			                    </tbody>
			                  </table>
			                   <button type="button" data-toggle="modal" data-target="#addModal" rel="tooltip" class="btn btn-info btn-round pull-right" data-original-title="" title="Add new Service">
				                            Add
				                            <i class="now-ui-icons ui-1_simple-add"></i>
				                </button>
			                </div>
			              </div>
			              @include('service/add')
             			  @include('service/edit')
			    </div>
			</div>
		</div>
	  </div>
</div>

<script>
	function placeValue(id,name,duration,price)
	{

		$('#eid').val(id);
		$('#ename').val(name);
		$('#eduration').val(duration);
		$('#eprice').val(price);

	}
	function deact(id,type)
	{
		if(type == 1)
			text = "deactivate";
		else
			text = "activate"
		swal({
	      title: 'Confirm',
	      text: "Are you sure you want to "+ text +" this service?",
	      type: 'warning',
	      showCancelButton: true,
	      confirmButtonColor: '#3085d6',
	      cancelButtonColor: '#d33',
	      confirmButtonText: text.toUpperCase(),
	    }).then((result) => {
			if(result){
				var form_data = {
					_token: $("input[name=_token]").val(),
					id:id,
					type:type
				};
				$.ajax({
			         url : "{{url('/')}}"+"/admin/sdeact",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            //console.log(msg);
			           
			            if(msg=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }

			            if(msg=="Success"){
			            	success("Service "+ text +"d successfully.");
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			}
			
	    })
	}
	function addService()
	{
   		var form_data = $("#addService").serialize();
   		
       	$.ajax({
	         url : "{{route('services.store')}}",
	         data :  form_data,
	         type : "POST",
	         success : function(msg){
	            //success();
	            //console.log(msg);
	            var res = msg.split('|');
	            if(res[0]=="Error"){
	                error("The system encountered an error. Please contact the administrator.");
	            }
	            if(res[0]=="Success"){
	            	success('Service added successfully.')
	            	setTimeout(function(){window.location.reload();},1500);
	            }

	        }
       });
       return false;
	}

   function editService()
   {
	   var form_data={
	   		_token: $("input[name=_token]").val(),
	   		id: $('#eid').val(),
	   		name: $('#ename').val(),
	   		duration: $('#eduration').val(),
	        price: $('#eprice').val(),
	        ajax: 1
	   };
    
	   $.ajax({
	    url : "{{url('/')}}"+"/admin/updateService",
	     data :  form_data,
	     type : "POST",
	    success : function(msg){
	        //success();
	        console.log(msg);
	        if(msg=="Error"){
	            error("The system encountered an error. Please contact the administrator.");
	        }
	        if(msg=="Success"){
	        	success('Service updated.')
	        	setTimeout(function(){window.location.reload();},1500);
	        }

	    }
	   }); 
       return false;
   }
</script>
@endsection