@extends('layouts.app')
@section('content')
 <div class="panel-header panel-header-sm"></div>
      <div class="content">
        <div class="row">
			<div class="col-md-12">
			            <div class="card">
			              <div class="card-header">
			                <h4 class="card-title"><i class="now-ui-icons users_circle-08"></i> Users</h4>
			              </div>
			              <div class="card-body">
			                <div class="table-responsive">
			                  <table id="datatable" class="table table-simple table-bordered" cellspacing="0" width="100%">
			                    <thead class=" text-primary">
			                      <tr align="center">
				                      <th>
				                        Username
				                      </th>
				                      <th>
				                        Alias
				                      </th>
				                      <th>
				                        Role
				                      </th>
				                      <th>
				                        Status
				                      </th>
				                      <th>
				                      	Actions
				                      </th>
			                      </tr>
			                    <tbody>
			                    	@if($users->count()>0)
				                    	@foreach($users as $row)
				                    		<tr class="text-center" >
						                        <td class="text-left">
						                           
						                           {{$row->username}}
						                        </td>
						                         <td class="text-left">
						                           {{strtoupper($row->name)}}
						                        </td>
						                        <td>
						                          {{$row->role->name}}
						                        </td>
						                        <td>
						                        	@if($row->is_active ==1)
						                        		<span class="text-success">Active</span>
						                        	@else
						                        		<span class="text-danger">Inactive</span>
						                        	@endif
						                        </td>
						                        <td >
						                        	@if($row->role_id != 1)
							                            <button @if($row->is_active != 1) hidden @endif type="button" rel="tooltip" title="Reset Password" class="btn btn-info btn-icon btn-sm " onclick="resetPass({{$row->id }})">
							                            <i class="now-ui-icons ui-2_settings-90"></i>
							                          	</button>
							                           @if($row->is_active == 1)
								                          <button type="button" rel="tooltip" title="Deactivate" onclick="deact({{$row->id}},1)" class="btn btn-danger btn-icon btn-sm ">
								                            <i class="now-ui-icons ui-1_simple-remove"></i>
								                          </button>
								                       @else
									                       <button type="button" rel="tooltip" title="Activate" onclick="deact({{$row->id}},2)" class="btn btn-success btn-icon btn-sm ">
									                            <i class="now-ui-icons ui-1_check"></i>
								                          </button>
								                       		
								                       @endif
								                    @else
								                    	
								                    @endif
						                        </td>
					                      	</tr>
				                    	@endforeach
				                    @else
				                    	<tr>
				                    		<td align="center" colspan="6">
				                    			<span class="text-primary">No data found</span>
				                    		</td>
				                    	</tr>
				                    @endif
			                    </tbody>
			                  </table>

			                   <button type="button" data-toggle="modal" onclick="getRole({{Helper::getRole()}})" data-target="#addModal" rel="tooltip" class="btn btn-info btn-round pull-right" data-original-title="" title="Add new Shift">
				                            Add
			                            <i class="now-ui-icons ui-1_simple-add"></i>
				                </button>
			                </div>
			              </div>
			            </div>
			  </div>
             @include('user/edit')
             @include('user/add')
		</div>
	</div>
	 <script type="text/javascript">
	 	function getRole(items)
	 	{
	 		if(items.length <= 0)
	 		{
	            output = '<option selected disabled><span class="text-danger">Please set up roles under settings.</span></option>';  
	        }else{
	            output = "<option selected disabled><span class='text-primary'>Please select a role.</span></option>";
	            $.each(items, function(key,value){          
	                output = output+ '<option value="'+value['id']+'">'+value['name']+'</option>';
	            });
	        }
            $("#role option").remove();
            $('#role').append(output);
	 	}

		function deact(id,type)
		{
			if(type == 1)
				text = "deactivate";
			else
				text = "activate"
			swal({
	          title: 'Confirm',
	          text: "Are you sure you want to "+ text +" this user?",
	          type: 'warning',
	          showCancelButton: true,
	          confirmButtonColor: '#3085d6',
	          cancelButtonColor: '#d33',
	          confirmButtonText: text.toUpperCase(),
	        }).then((result) => {
				if(result){
					var form_data = {
						_token: $("input[name=_token]").val(),
						id:id,
						type:type
					};
					$.ajax({
				         url : "{{url('/')}}"+"/admin/deact_user",
				         data :  form_data,
				         type : "POST",
				        success : function(msg){
				            //success();
				            //console.log(msg);
				           
				            if(msg=="Error"){
				                error("The system encountered an error. Please contact the administrator.");
				            }

				            if(msg=="Success"){
				            	success("Shift "+ text +"d successfully.");
				            	setTimeout(function(){window.location.reload();},1500);
				            }

				        }
				       }); 
				}
				
	        })
		}

		//document.getElementById("beg_date").onchange = function() {myFunction()};
	    function addUser()
	    {
	       if($('#role').val() == "" ){
	       		error("Please select a role.");
	       }else
	       {
		       var form_data={
		       		_token: $("input[name=_token]").val(),
		       		username: $('#username').val(),
		       		name: $('#name').val(),
		            role: $('#role').val(),
		            ajax: 1
		       };
		        
		       $.ajax({
		         url : "{{route('users.store')}}",
		         data :  form_data,
		         type : "POST",
		        success : function(msg){
		            var res = msg.split('|');
		            if(res[0]=="Error"){
		            	if(res[1]!="")
		            		error(res[1]);
		            	else
		                	error("The system encountered an error. Please contact the administrator.");
		            }
		            if(res[0]=="Success"){
		            	success('User added successfully.')
		            	setTimeout(function(){window.location.reload();},1500);
		            }

		        }
		       });
	       }
	       return false;
       }

       function resetPass(id)
       {
	       var form_data={
	       		_token: $("input[name=_token]").val(),
	       		id: id,
	            ajax: 1
	       };
	    	swal({
	          title: 'Confirm',
	          text: "Are you sure you want to reset the Password of this user?",
	          type: 'warning',
	          showCancelButton: true,
	          confirmButtonColor: '#3085d6',
	          cancelButtonColor: '#d33',
	          confirmButtonText: 'Reset',
	        }).then((result) => {
	        	if(result)
	        	{
		        	 $.ajax({
				        url : "{{url('/')}}"+"/admin/resetPass",
				         data :  form_data,
				         type : "POST",
				        success : function(msg){
				            //success();
				            console.log(msg);
				            if(msg=="Error"){
				                error("The system encountered an error. Please contact the administrator.");
				            }
				            if(msg=="Success"){
				            	success('Password successfully reset.');
				            	setTimeout(function(){window.location.reload();},1500);
				            }

				        }
				     });

	        	}
	        });
       	  return false;
       }

  </script>	
@endsection

@section('extrajs')
 <script>
    $(document).ready(function() {
      $('#datatable').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });


      func.initDateTimePicker();
      if ($('.slider').length != 0) {
        func.initSliders();
      }
    });
</script>
@endsection