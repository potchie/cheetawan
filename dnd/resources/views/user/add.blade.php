<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-notice">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
	          <i class="now-ui-icons ui-1_simple-remove"></i>
	        </button>
    		<h5 class="modal-title" id="myModalLabel">Add new User</h5>
      	</div>
      	<div class="modal-body">
      		<form method="POST" onsubmit="return addUser()">
			    @csrf
			    <div class="instruction">
	              <div class="input-group no-border">
	                <input name="username" id="username" type="text" required class="form-control" placeholder="Username">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Username"></i>
	                  </div>
	                </div>
	              </div>
	              <div class="input-group no-border">
	                <input name="name" id="name" type="text" value="" required class="form-control" placeholder="Alias">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Alias"></i>
	                  </div>
	                </div>
	              </div>
				    <div class="input-group no-border">
		               <select id="role" name="role" class="form-control" required data-size="7" data-style="btn btn-round" title="role">
                          <option disabled selected>Role</option>

                        </select>
					</div>
		            <div class="input-group no-border">
		                <div>
		                	<span style="font-size: xx-small; color: red;">
		                		** The user will be registered with default passwod: 123456.
		                	</span>
		            	</div>
	              	 </div>
              	</div>
          	    <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
        		</div>
			</form>
      	</div>
    </div>
  </div>
</div>

