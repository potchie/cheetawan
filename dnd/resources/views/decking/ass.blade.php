<div class="row">
	<table id="tbldeck" border="0" width="100%">
		<thead>
			<tr>
				<th>No.</th>
				<th>Customer Name</th>
				<th>Service</th>
				<th>Room</th>
				<th>Personnel</th>
				<th>Actions</th>
			</tr>
		</thead>
		@foreach($ass as $key=>$row)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$row->cschedule->lname}}a {{$row->cschedule->fname}}</td>
				<td>{{$row->cschedule->service->duration}} mins.- {{$row->cschedule->service->name}}</td>
				<td>{{$row->room->name}}</td>
				<td>{{$row->p_name}}</td>
				<td><button class="btn btn-icon btn-round btn-success" onclick="clearRoom({{$row->id}})">
                      <i class="now-ui-icons sport_user-run"></i>
                    </button>
                </td>
			</tr>		
		@endforeach
	</table>

</div>

<script>
	function clearRoom(id){
          $.ajax({
           url : "{{url('/')}}"+"/admin/rooms/clear/"+ id,
           success : function(msg){
              //success();
              console.log(msg);
              var res = msg.split('|');
              if(res[0]=="Error"){
                  error("The system encountered an error. Please contact the administrator.");
              }
              if(res[0]=="Success"){
                success('Job request has been tagged as compeleted.')
                setTimeout(function(){window.location.reload();},1500);
              }

          }
       });
       return false;
	}
</script>
