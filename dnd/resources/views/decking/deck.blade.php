<div>
  <button class="btn btn-icon btn-round btn-success" onclick="refreshData();">
      <i class="fab fa-twitter"></i>
  </button>
	<table id="tbldeck" border="0" width="100%">
		@foreach($personnel as $key=>$row)
			<tr>
				<td>{{$row->personnel->fname}} {{$row->personnel->lname}}</td>
				<td><button @if($row->status!=1)disabled @endif class="btn btn-primary btn-round" data-toggle="modal" onclick="pullData({{$row->personnel->id}} ,'{{$row->personnel->lname}} @if($row->personnel->mname){{$row->personnel->mname[0]}} @endif {{$row->personnel->fname}}')" data-target="#addModal" rel="tooltip">assign</button></td>
			</tr>
		@endforeach
	</table>

	<!-- modal start -->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-notice">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h5 class="modal-title" id="myModalLabel">Assign Task</h5>
              </div>
              <div class="modal-body">
              	<form id="addAssignment" method="post" onsubmit="return addAssignment();">
              	<input hidden id="_token" name="_token" value="{{ Session::token() }}">
                <div class="instruction">
                	  <input name="aid" id="aid" type="text" value="" required class="form-control" hidden>
                    <input name="aname" id="aname" type="text" value="" required class="form-control" hidden>
		              <div class="input-group no-border">
		                	<select id="aoptRoom" name="aoptRoom" class="form-control" required>
		                	</select>
		                	<select id="aoptSched" name="aoptSched" class="form-control" required>
		                		
		                	</select>
		              </div>
                <p></p>
              </div>
              <div class="modal-footer justify-content-center">
                <button class="btn btn-info btn-round" type="submit">Save <i class="now-ui-icons ui-1_send"></i></button>
              </div>
              </form>
            </div>
          </div>
		 </div>
		</div>
			<!-- modal end -->
</div>

<script>
	function addAssignment(){
		  var form_data = $("#addAssignment").serialize();
      if($("#aoptSched").val() == null || $("#aoptRoom").val() == null ){
          error("Please complete the fields.");
      }else{
          $.ajax({
           url : "{{route('assignment.store')}}",
           data :  form_data,
           type : "POST",
           success : function(msg){
              //success();
              console.log(msg);
              var res = msg.split('|');
              if(res[0]=="Error"){
                  error("The system encountered an error. Please contact the administrator.");
              }
              if(res[0]=="Success"){
                success('Assignment added successfully.')
                setTimeout(function(){window.location.reload();},1500);
              }

          }
       });
      }
       return false;
	}

  function refreshData()
  {
     $.ajax({
              url : "{{url('/')}}"+"/admin/getDeck",
              success : function(items){
                  if(items.length <= 0)
                    output = '<tr><td align="center" colspan="2"><span class="text-primary">No Personnel Scheduled at the moment.</span></td></tr>';  
                  else{
                    output =""
                      $.each(items, function(key,value){ 
                          output = output +  '<tr><td>';
                          output = output + value['personnel']['fname'] + ' ' + value['personnel']['lname'] + '</td><td>';
                          output = output + '<button ';
                          if(value['status']!=1){
                              output=output+' disabled ';
                          }

                           output = output + 'class="btn btn-primary btn-round" data-toggle="modal" onclick="pullData(' + value['personnel']['id']+ ',\''+ value['personnel']['lname'] + ' ' +value['personnel']['fname'] + '\' )" data-target="#addModal" rel="tooltip"> assign </button></td></tr>';
                      }); 
                    }
                $("#tbldeck tbody tr").remove();
                $('#tbldeck').append(output);
              }
         }); 
  }

	function pullData(id,name)
	{
		 document.getElementById('aid').value = id;
     document.getElementById('aname').value = name;
		 $.ajax({
              url : "{{url('/')}}"+"/admin/getRooms",
              success : function(items){
                  if(items.length <= 0)
                    output = '<option selected disabled><span class="text-danger">No Rooms available</span></option>';  
                  else{
                        output = "<option selected disabled><span class='text-primary'>Please select a room.</span></option>";
                        $.each(items, function(key,value){          
                            output = output+ '<option value="'+value['id']+'">'+value['name']+'</option>';
                        });
                    }
                $("#aoptRoom option").remove();
                $('#aoptRoom').append(output);
              }
         });
         $.ajax({
              url : "{{url('/')}}"+"/admin/getCschedule",
              success : function(items){
                  if(items.length <= 0)
                    output = '<option selected disabled><span class="text-danger">No schedule yet</span></option>';  
                  else{
                        output = "<option selected disabled><span class='text-primary'>Please select an active schedule.</span></option>";
                        $.each(items, function(key,value){          
                            output = output+ '<option value="'+value['id']+'">'+value['lname']+', '+value['fname']+' ('+value['service']['name']+'-'+value['service']['duration'] +'min. )</option>';
                            console.log(value);
                        });

                    }
                $('#aoptSched option').remove();
                $('#aoptSched').append(output);
              }
         }); 

	}

</script>
