@extends('layouts.app')

@section('content')
    <div class="panel-header panel-header-lg">
        <canvas id="bigDashboardChart"></canvas>
     </div>
     <div class="content">
        <div class="row">
            <div class="col-md-12">
            <div class="card card-stats">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="statistics">
                      <div class="info">
                        <div class="icon icon-primary">
                          <i class="now-ui-icons ui-2_chat-round"></i>
                        </div>
                        <h3 class="info-title">{{Helper::getHomeStat(0)}}</h3>
                        <h6 class="stats-title">Active Schedule</h6>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="statistics">
                      <div class="info">
                        <div class="icon icon-info">
                          <i class="now-ui-icons users_single-02"></i>
                        </div>
                        <h3 class="info-title">{{Helper::getHomeStat(1)}}</h3>
                        <h6 class="stats-title">Todays Customer</h6>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="statistics">
                      <div class="info">
                        <div class="icon icon-danger">
                          <i class="now-ui-icons objects_support-17"></i>
                        </div>
                        <h3 class="info-title">{{Helper::getHomeStat(2)}}</h3>
                        <h6 class="stats-title">Todays Book Requests</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
             <div class="col-lg-4 col-md-6">
                <div class="card card-chart">
                    
                </div>
            </div>
        </div>
       
        <div class="col-md-12">
          <div class="card card-stats">
            <div class="card-body">
              <div class="row">
                   <div class="col-md-6">
                    <div class="statistics">
                      <div class="info">
                        <div class="stats-title text-primary">
                            <div>Personnel on Deck </div>
                        </div>
                         @include('decking.deck')
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="statistics">
                      <div class="info">
                        <div class="stats-title text-primary">On-Going Services</div>
                         @include('decking.ass')
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
    </div>


@endsection


@section('extrajs')
    <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      var jan = "{{Helper::getData(1)}}";
      var feb = "{{Helper::getData(2)}}";
      var mar = "{{Helper::getData(3)}}";
      var apr = "{{Helper::getData(4)}}";
      var may = "{{Helper::getData(5)}}";
      var jun = "{{Helper::getData(6)}}";
      var jul = "{{Helper::getData(7)}}";
      var aug = "{{Helper::getData(8)}}";
      var sep = "{{Helper::getData(9)}}";
      var oct = "{{Helper::getData(10)}}";
      var nov = "{{Helper::getData(11)}}";
      var dec = "{{Helper::getData(12)}}";

      func.initDashboardPageCharts(jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec);

      // demo.initVectorMap();

    });
  </script>

 <script src="{{asset('fullcalendar/moment.min.js')}}"></script>
 <script src="{{asset('fullcalendar/fullcalendar.min.js')}}"></script>
{!! $calendar->script() !!}
@endsection

