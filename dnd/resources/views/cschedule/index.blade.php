@extends('layouts.app')

@section('content')

	 <div class="panel-header panel-header-sm"></div>
      <div class="content">
				<div class="col-md-12 mr-auto ml-auto">
          <!--      Wizard container        -->
          <div class="wizard-container">
            <div class="card card-wizard" data-color="primary" id="wizardProfile">
                <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="card-header text-center" data-background-color="orange">
                  <h3 class="card-title">
                    Customer Schedule
                  </h3>
                  <h3 class="description"></h3>
                    <div class="wizard-navigation">
                      <ul class="nav nav-pills">
                        <li class="nav-item">
                          <a class="nav-link active" href="#calendar" data-toggle="tab" data-toggle="tab" role="tab" aria-controls="address" aria-selected="false">
                            <i class="now-ui-icons ui-1_calendar-60"></i> Calendar
                          </a>

                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#today" data-toggle="tab" role="tab" aria-controls="about" aria-selected="true">
                            <i class="now-ui-icons location_bookmark"></i> Today
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#new" data-toggle="tab" onclick="refreshdata()" data-toggle="tab" role="tab" aria-controls="account" aria-selected="false">
                            <i class="now-ui-icons ui-1_bell-53"></i> New <span class="pull-right" style="color:red;" id="notifdata2" > {{Helper::getNewBook()}}</span>
                          </a>
                        </li>
                        
                      </ul>
                    </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    @include('cschedule.timeverify')
                    <div class="tab-pane show fade" id="today">
                        @include('cschedule.today')
                    </div>
                     <div class="tab-pane show fade" id="new">
                        @include('cschedule.approval')
                    </div>
                    <div class="tab-pane show active" id="calendar">
                      <div class="col-md-12">
                        <div class="card">
                          <div class="card-header">
                            <h4 class="card-title"><i class="now-ui-icons ui-1_calendar-60"></i> Customer Schedule</h4>
                          </div>
                          <div class="card-body">
                             <div class="pull-right" style="float: right;">
                              <a data-toggle="modal" data-target="#selectmonth" type="button" rel="tooltip" class="btn btn-success btn-round pull-right" data-original-title="" title="Print Monthly Schedule">
                                            Print
                                            <i class="now-ui-icons ui-1_simple-add"></i>
                                  </a>
                            </div>
                            <div style="float: right;">
                              @include('calendar.fullcalendar')

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
      
                  </div><!-- tab content -->
              </div><!-- Card Body -->
              @include('cschedule.selectmonth')
          </div><!-- wizard container -->
			</div>	
	 </div>
  </div>
@endsection

@section('extrajs')

<script>
    function convertTime(time)
    {
       /*Start format time*/
            stime ="";
            etime = "";
            dstime = time.split(":");
            sappend = "AM";
            shour = dstime[0];
            if (shour > 12){
                shour -= 12;
                sappend = "PM";
            }
            return stime =  shour +":"+dstime[1] + " " + sappend;

    }

    function refreshdata(){

         $.ajax({
              url : "{{url('/')}}"+"/admin/testme",
              success : function(items){
                  if(items.length <= 0)
                    output = '<tr><td align="center" colspan="6"><span class="text-primary">No data found</span></td></tr>';  
                  else{
                        output = "";
                        $.each(items, function(key,value){          
                            output = output+ '<tr class="text-center">';
                            output = output+     '<td class="text-left">';
                            output = output+      value['fname'] + ' ';
                            if(value['mname']!=null) 
                                output = output + value['mname'].charAt(0) + '. ';
                            output = output + value['lname'] + '</td><td>';

                            
                            /*End format time*/
                            output = output +     value['date'] + '</td><td>' + convertTime(value['start']) +'-' + convertTime(value['end']) + '</td><td class="text-right">'+ value['service']['duration'] + 'mins. - '+ value['service']['name'] + '</td><td class="text-right">For Approvals</td><td>';  
                           

                            output = output + '<button type="button" rel="tooltip" title="Approve" data-toggle="modal" data-target="#timeverify" rel="tooltip" class="btn btn-success btn-icon btn-sm" onclick="verifyMe(' + value['id'] +','+ value['service']['duration'] + ',\'' + value['start'] +'\',\''+ value['end'] +'\',\''+ value['service']['name'] +'\' )"><i class="now-ui-icons ui-1_check"></i></button> <button type="button" rel="tooltip" title="Deny" onclick="deny('+ value['id'] +')" class="btn btn-danger btn-icon btn-sm "><i class="now-ui-icons ui-1_simple-remove"></i></button></td></tr>';


                        });
                    }
                $("#datatable tbody tr").remove();
                $('#datatable').append(output);
              }
         }); 
    }


    $(document).ready(function() {
      // Initialise the wizard
      func.initNowUiWizard();
      setTimeout(function() {
        $('.card.card-wizard').addClass('active');
      }, 600);


      $('#datatable').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

      $('#datatable1').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      var table = $('#datatable1').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        if ($($tr).hasClass('child')) {
          $tr = $tr.prev('.parent');
        }
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

    });

    var source = new EventSource('{{url('/admin/notifs')}}');
       source.addEventListener('notif', function(event) {
           var dt  = JSON.parse(event.data);
              console.log(dt.ns);
              if(dt.ns >=1){
                document.getElementById("notifdata2").style.visibility = "visible";
                document.getElementById("notifdata2").innerHTML = dt.ns;
              }else{
                document.getElementById("notifdata2").style.visibility = "hidden";
              }
      }, false); 
</script>
	 <script src="{{asset('fullcalendar/moment.min.js')}}"></script>
	 <script src="{{asset('fullcalendar/fullcalendar.min.js')}}"></script>
	 {!! $calendar->script() !!}
@endsection