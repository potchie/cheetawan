<div class="modal fade" id="addCSched" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-notice">
    <div class="modal-content">
		<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
	          <i class="now-ui-icons ui-1_simple-remove"></i>
	        </button>
	        <h5 class="modal-title" id="myModalLabel">Walk - in Client</h5>
      	</div>
      	<div class="modal-body">
      	<form id="contactForm" onsubmit="return book();" name="contactForm">
	        <input hidden id="_token" name="_token" value="{{ Session::token() }}">
	        <div class="row">
	          <div class="col-md-6">
	            <div class="form-group">
	               <label class="text-white">First Name</label>
	              <input class="form-control" id="fname" name="fname" type="text" placeholder="Given Name*" required data-validation-required-message="Please enter your Given Name.">
	              <p class="help-block text-danger"></p>
	            </div>
	            <div class="form-group">
	               <label class="text-white">Middle Name</label>
	               <input class="form-control" id="mname" name="mname" type="text" placeholder="Middle Name (Optional)" >
	              <p class="help-block text-danger"></p>
	            </div>
	            <div class="form-group">
	               <label class="text-white">Last Name</label>
	              <input class="form-control" id="lname" name="lname" type="text" placeholder="Last Name*" required data-validation-required-message="Please enter your Last name.">
	              <p class="help-block text-danger"></p>
	            </div>
	            <div class="form-group">
	               <label class="text-white">Contact Number</label>
	              <input class="form-control" name="contact" id="contact" type="number" placeholder="Mobile Number *" required data-validation-required-message="Please enter your email address.">
	              <p class="help-block text-danger"></p>
	            </div>
	          </div>
	          <div class="col-md-6">
	            <div class="form-group">
	              <label class="text-white">Date</label>
	              <input class="form-control" name="date" id="date" type="date" placeholder="Date" value="{{date('Y-m-d')}}" required>
	              <p class="help-block text-danger"></p>
	            </div>
	             <div class="form-group">
	              <label class="text-white">Time</label>
	              <input class="form-control" name="start" id="start" type="time" placeholder="Time" required>
	              <p class="help-block text-danger"></p>
	            </div>
	           @php $services = Helper::getServices() @endphp
	            <div class="form-group">
	              <label class="text-white">Service</label>
	              <select class="form-control" name="service" id="service"  required data-validation-required-message="Please select a service.">
	                  <option disabled selected>Please Select a Service.</option>
	                  @foreach($services as $row)
	                      <option value="{{ $row->id }}"> {{$row->duration}} min. - {{$row->name}}</option>
	                  @endforeach
	              </select>
	              <p class="help-block text-danger"></p>
	            </div>
	          </div>
	          <div class="clearfix"></div>
	          <div class="col-lg-12 text-center">
	            <div id="success"></div>
	            <button id="sendMessageButton" class="btn btn-success btn-xl text-uppercase" type="submit">Book</button>
	          </div>
	        </div>
	      </form>
	   </div>
    </div>
  </div>
</div>

<script>
	function book(){
       var form_data = $("#contactForm").serialize();
       if($("#service").val() == null){
          error("Please select a service.");
        }else{
          $.ajax({
             url : "{{url('/')}}"+"/saveCSchedA",
             data :  form_data,
             type : "POST",
            success : function(msg){
                console.log(msg);
                  if(msg == "Success"){
  					success("Walkin client schedule added.");
    				setTimeout(function(){window.location.reload();},1500);
                  }else{
                    error('Something went wrong. Please contact your system administrator.');
                  }
                }
           });
          
        }
        return false;
     }
</script>