<div class="modal fade" id="timeverify" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-notice">
        <div class="modal-content">
          <form id="frm_timeverify" onsubmit="return updateTime();">
          	<input hidden id="_token" name="_token" value="{{ Session::token() }}">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <h5 class="modal-title" id="myModalLabel">Verify Time Slot</h5>
          </div>
          <div class="modal-body">
            <div class="instruction">
            	  <div class="input-group no-border" hidden>
	                <input name="vid" id="vid" type="text" required class="form-control">
	              </div>
	              <div class="input-group no-border" hidden>
	                <div class="input-group no-border">
	                <input name="vduration" id="vduration" type="text" required class="form-control">
	              </div>
	              </div>
	               <div class="input-group no-border">
	               <input name="vService" id="vservice" type="text" class="form-control" disabled>
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Service"></i>
	                  </div>
	                </div>
	              </div>		
	              <div class="input-group no-border">
	                <input name="vstart" id="vstart" type="time" required class="form-control" onchange ="timeCalc()">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="Start Time"></i>
	                  </div>
	                </div>
	              </div>
	              <div class="input-group no-border">
	               <input name="vdisplay" id="vdisplay" type="time" required class="form-control" disabled>
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="End time"></i>
	                  </div>
	                </div>
	              </div>
	              <div class="input-group no-border" hidden>
	               <input name="vend" id="vend" type="time" required class="form-control">
	                <div class="input-group-append">
	                  <div class="input-group-text">
	                    <i class="now-ui-icons travel_info" rel="tooltip" title="End time"></i>
	                  </div>
	                </div>
	              </div>							          
            </div>
            <p></p>
          </div>
          <div class="modal-footer justify-content-center">
            <button class="btn btn-info btn-round">Save <i class="now-ui-icons ui-1_send"></i></button>
          </div>
          </form>
        </div>
      </div>
</div>

<script >
	function timeCalc()
	{
		curTime = $("#vstart").val();
		duration = $("#vduration").val();
		$("#vdisplay").val(moment.utc(curTime,'HH:mm').add(duration,'minutes').format('HH:mm'));
		$("#vend").val($('#vdisplay').val());
	}

	function updateTime()
	{
		var form_data = $("#frm_timeverify").serialize();
       	$.ajax({
	         url : "{{url('/')}}"+"/admin/verifyTime",
	         data :  form_data,
	         type : "POST",
	         success : function(msg){
	         	// console.log(msg);

	         	if(msg == "Success"){
            		success("Successfully booked!");
            		setTimeout(function(){window.location.reload();},1500);
            	}else{
            		swal({
					  type: 'error',
					  title: 'Oops...',
					  text: "An error occured while saving... Please contact your system administrator.",
					  });
				}        	
		    }
        		
       });
       return false;
	}

</script>