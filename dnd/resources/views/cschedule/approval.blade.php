			<div class="col-md-12">
			            <div class="card">
			              <div class="card-header">
			                <h4 class="card-title"><i class="now-ui-icons business_badge"></i> New Booking</h4>
			              </div>
			              <div class="card-body">
			                <div class="table-responsive">
			                  <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
			                    <thead class=" text-primary">
			                      <tr align="center">
				                      <th>
				                        Name
				                      </th>
				                      <th>
				                        Date
				                      </th>
				                      <th>
				                        Time
				                      </th>
				                      <th>
				                        Service
				                      </th>
				                      <th>
				                        Status
				                      </th>
				                      <th>
				                      	Actions
				                      </th>
			                      </tr>
			                    </thead>
			                    <tbody id="tablereq">
			                    	
			                    	@if($approval->count()>0)
				                    	@foreach($approval as $row)
				                    		<tr class="text-center">
						                        <td class="text-left">
						                           {{$row->fname}} {{$row->mname[0]}}. {{$row->lname}}
						                        </td>
						                        <td>
						                          {{$row->date}}
						                        </td>
						                        <td>
						                          {{$row->start}} - {{$row->end}}
						                        </td>
						                        <td class="text-right">
													{{$row->service->duration}} mins. - {{$row->service->name}}
						                        </td>
						                        <td class="text-right">
						                        	For Approval
														<!-- 0-for approval
												        	1- active
												        	2- on-going
												        	3 - finished
												        	4 - cancelled -->
						                        </td>
						                        <td>
						                          
						                          <a @if($row->status != 1) hidden @endif type="button" rel="tooltip" title="Profile" class="btn btn-info btn-icon btn-sm" href="{{url('/admin/personnel',[$row->id])}}">
						                            <i class="now-ui-icons users_single-02"></i>
						                          </a>
						                          <button  @if($row->status != 1) hidden @endif type="button" rel="tooltip" title="Edit" data-toggle="modal" data-target="#editModal" class="btn btn-tumblr btn-icon btn-sm " 
						                          		onclick="placeValue( {{$row->id}} ,'{{$row->fname}}','{{$row->mname}}','{{$row->lname}}',{{$row->age}},'{{$row->gender}}', {{$row->salary}} ) "
						                            >
						                            <i class="now-ui-icons ui-2_settings-90"></i>
						                          </button>
								                       <button type="button" rel="tooltip" title="Approve" data-toggle="modal" data-target="#timeverify" rel="tooltip" onclick="verifyMe({{$row->id}},{{$row->service->duration}},'{{ $row->start }}','{{ $row->end }}','{{ $row->service->name}}' )" class="btn btn-success btn-icon btn-sm">
								                            <i class="now-ui-icons ui-1_check"></i>
							                          </button>
							                          <button type="button" rel="tooltip" title="Deny" onclick="deny({{$row->id}})" class="btn btn-danger btn-icon btn-sm ">
							                            <i class="now-ui-icons ui-1_simple-remove"></i>
							                          </button>
						                        </td>
					                      	</tr>
				                    	@endforeach
				                    @else
				                    	<tr>
				                    		<td align="center" colspan="6">
				                    			<span class="text-primary">No data found</span>
				                    		</td>
				                    	</tr>
				                    @endif
			                    </tbody>
			                  </table>
			                </div>
			              </div>
			            </div>
			  </div>
             @include('cschedule/timeverify')
	 <script type="text/javascript">
	 			function verifyMe(id,duration,start,end,service){
	 					$("#vid").val(id);
	 					$("#vduration").val(duration);
	 					$("#vstart").val(start);
	 					$("#vend").val(end);
	 					$("#vdisplay").val(end);
	 					$("#vservice").val(duration+"mins -"+service);
	 			}

	 			function deny(id)
	 			{
	 				swal({
			          title: 'Confirm',
			          text: "Are you sure you want to deny this booking?",
			          type: 'warning',
			          showCancelButton: true,
			          confirmButtonColor: '#3085d6',
			          cancelButtonColor: '#d33',
			          confirmButtonText: "Deny",
			        }).then((result) => {
						if(result){
							var form_data = {
								_token: $("input[name=_token]").val(),
								id:id
							};
							$.ajax({
						         url : "{{url('/')}}"+"/admin/deny",
						         data :  form_data,
						         type : "POST",
						        success : function(msg){
						            //success();
						            //console.log(msg);
						           
						           
						            if(msg=="Success"){
						            	success("Booking denied successfully.");
						            	setTimeout(function(){window.location.reload();},1500);
						            }else{
						            	text = msg.split("|")
						           	     error("The system encountered an error. Please contact the administrator." + text[0]);
						           
						            }

						        }
						       }); 
						}
						
			        })
	 			}

	 			function placeValue(id,fname,mname,lname,age,gender,salary)
	 			{

	 				$('#eid').val(id);
	 				$('#efname').val(fname);
	 				$('#emname').val(mname);
	 				$('#elname').val(lname);
	 				$('#eage').val(age);
	 				$('#egender').val(gender);
	 				$('#esalary').val(salary);

	 			}
    //document.getElementById("beg_date").onchange = function() {myFunction()};
			    function add_Personnel(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		fname: $('#fname').val(),
			       		mname: $('#mname').val(),
			            lname: $('#lname').val(),
			            age: $('#age').val(),
			            gender: $('#gender').val(),
			            salary: $('#salary').val(),
			            ajax: 1
			       };
			       
			       if($('#gender').val()){
						       $.ajax({
						         url : "{{route('personnel.store')}}",
						         data :  form_data,
						         type : "POST",
						        success : function(msg){
						            //success();
						            //console.log(msg);
						            var res = msg.split('|');
						            if(res[0]=="Error"){
						                error("The system encountered an error. Please contact the administrator.");
						            }
						            if(res[0]=="Success"){
						            	success('Personnel added successfully.')
						            	setTimeout(function(){window.location.reload();},1500);
						            }

						        }
						       });
					}else{
						error('Please select a gender.');
					}

			           return false;
			       }

			       function edit_Personnel(){
			       var form_data={
			       		_token: $("input[name=_token]").val(),
			       		id: $('#eid').val(),
			       		fname: $('#efname').val(),
			       		mname: $('#emname').val(),
			            lname: $('#elname').val(),
			            age: $('#eage').val(),
			            gender: $('#egender').val(),
			            salary: $('#esalary').val(),
			            ajax: 1
			       };
			        
			       $.ajax({
			        url : "{{url('/')}}"+"/admin/updatePersonnel",
			         data :  form_data,
			         type : "POST",
			        success : function(msg){
			            //success();
			            console.log(msg);
			            if(msg=="Error"){
			                error("The system encountered an error. Please contact the administrator.");
			            }
			            if(msg=="Success"){
			            	success('Personnel updated.')
			            	setTimeout(function(){window.location.reload();},1500);
			            }

			        }
			       }); 
			           return false;
			       }

  </script>	
