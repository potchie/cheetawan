<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['prefix'=>'admin','middleware'=>'auth'],function () {
	Route::get('/','HomeController@index');
	Route::get('getDeck','HomeController@getDeck');

	//personnel
	Route::resource('personnel','PersonnelController');
	Route::any('updatePersonnel','PersonnelController@updatePersonnel');
	Route::any('deact','PersonnelController@deact');
	Route::any('profile/{id}','PersonnelController@show_prof');
	Route::any('updateUser','UserController@updateUser');
		
	//Personnel schedule
	Route::resource('Psched','PscheduleController');
	Route::any('pSchedMany','PscheduleController@pSchedMany');

	//customer schedule + booking
	Route::resource('Csched','ScheduleController');
	Route::any('booking','ScheduleController@booking');
	Route::any('verifyTime','ScheduleController@verifyTime');
	Route::any('deny','ScheduleController@deny');
	
	//admin only links
	Route::middleware('Checkrole')->group(function () {
		

		//shift
		Route::resource('shift','ShiftController');
		Route::any('updateShift','ShiftController@updateShift');
		Route::any('deact_shift','ShiftController@deact');
			
		//services
		Route::resource('services','ServiceController');
		Route::any('sdeact','ServiceController@sdeact');
		Route::any('updateService','ServiceController@updateService');
		
		//rooms
		Route::resource('rooms','RoomController');
		Route::any('rdeact','RoomController@rdeact');
		Route::any('updateRoom','RoomController@updateRoom');


		//user
		Route::resource('users','UserController',['except'=>['show']]);
		Route::any('backup','UserController@backup');
		
		
		// Route::resource('monkeys', 'MonkeysController', [
		// 	'except' => ['edit', 'create']
		// ]);

		Route::any('deact_user','UserController@deact');
		Route::any('checkUsername', 'UserController@checkUsername');
		Route::any('resetPass', 'UserController@resetPass');
		
		Route::any('databackup','DatabaseBackupController@generateBackup');
	});

	//end admin only links
	
	Route::resource('users', 'UserController', [
			'only' => [ 'show']
	]);

	Route::any('changePass','UserController@changePass');

	Route::get('/rooms/clear/{id}','RoomController@clearRoom');
	Route::any('getTime','ShiftController@getTime');
	Route::any('saveSched','PscheduleController@saveSched');
	Route::any('savebSched','PscheduleController@savebSched');
	Route::any('editshed/{id}','PscheduleController@editshed');
	Route::any('editSched','PscheduleController@editSched');
	//printables
	Route::get('monthlysched/{month}','ReportController@monthlysched');
	Route::get('monthlyCsched/{month}','ReportController@monthlyCsched');

	//notifs
	Route::resource('/notifs', 'NotifController');
	Route::any('/toText', 'NotifController@toText');

	// functions
	Route::any('/getDeck','HomeController@getDeck');
	Route::any('/getRooms','HomeController@getRooms');
	Route::any('/getCschedule','HomeController@getCschedule');

	//test link
	Route::any('/testme','ScheduleController@testme');

	//Assignment
	Route::resource('/assignment','AssignmentController');

	//Deck

	
});


Route::get('/', function () {
      return view('welcome');
    // return redirect()->route('login');
});
// Route::get('/','HomeController@home');
Route::any('/saveCSchedH','HomeController@saveCschedH');
Route::any('/saveCSchedA','HomeController@saveCschedA');

