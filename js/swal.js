function success(msg){
	swal({
  	position: 'center',
  	type: 'success',
  	title: 'Success',
    text: msg,
  	showConfirmButton: false,
  	timer: 2000
	});
}

function error(msg){
  swal({
  type: 'error',
  title: 'Oops...',
  text: msg,
  });
}

function successOk(msg){
     swal({
      title: 'Information',
      type: 'info',
      html:msg,
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, Ok!'
    })
}
function information(msg){
     swal({
      title: 'Information',
      type: 'info',
      html:msg,
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, Ok!'
    })
}

function loggedin(user){
  const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  toast({
    type: 'success',
    title: user + 'signed in successfully'
  })
}
