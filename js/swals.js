swals = {
    addPersonnel: function(){
          swal({
            title: 'Add Personnel',
            html: '<div class="form-group">' +
                    '<div class="row">'+
                       '<input id="input-field" type="text" class="form-control" />' +
                       '<input id="input-field" type="text" class="form-control" />' +
                       '<input id="input-field" type="text" class="form-control" />' +
                    '</div>' +
                  '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
          }).then(function(result) {
            swal({
              type: 'success',
              html: 'You entered: <strong>' +
                $('#input-field').val() +
                '</strong>',
              confirmButtonClass: 'btn btn-success',
              buttonsStyling: false

            }).catch(swal.noop);
          }).catch(swal.noop);
    }
}

function success(msg){
  swal({
    position: 'center',
    type: 'success',
    title: 'Success',
    text: msg,
    showConfirmButton: false,
    timer: 2000
  });
}

function error(msg){
  swal({
  type: 'error',
  title: 'Oops...',
  text: msg,
  });
}

function successOk(msg){
     swal({
      title: 'Information',
      type: 'info',
      html:msg,
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, Ok!'
    })
}

function information(msg){
     swal({
      title: 'Information',
      type: 'info',
      html:msg,
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, Ok!'
    })
}

function loggedin(user){
  const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  toast({
    type: 'success',
    title: user + 'signed in successfully'
  })
}